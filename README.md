# TRIAL

## Clone repository

To clone repository run `git clone https://gitlab.com/trial6924008/trial`.


## Install dependencies

1. Run `cd trial` to go app directory.
2. Run `npm ci` to install dependencies.

## Setting up database

1. Run `npm run createKey` to create key for database.
2. Run `npm run createNetwork` to create docker network for app and database.
3. Run `npm run db:start` to start database in docker container.
4. Run `npm run db:configure` to configure replica set for database.
5. Run `npm run db:migrate` to set up indexes and collections in database.
6. Run `npm run db:generate` to generate client code and types for database.
7. Run `npm run db:populate` to populate database by testing data.

## Build the app

1. Run `npm run build:next` to build client app.
2. Run `npm run build` to build server app.

## Containering of the app

1. Run `docker-compose build` to build app container.

## Launching outside the container

1. Run `npm run start:prod`. After that app will be lauched on port, which specified in `.env` file. Open `http://localhost:PORT` on your browser to see the app running (by default, port 3000 is defined in the .env file, so the app is available at `http://localhost:3000`). To see API documentation open `http://localhost:PORT/doc`.

## Launching inside the container

1. Run `npm run start:container`. After that app will be lauched on port, which specified in `.env` file. Open `http://localhost:PORT` on your browser to see the app running (by default, port 3000 is defined in the .env file, so the app is available at `http://localhost:3000`). To see API documentation open `http://localhost:PORT/doc`.