const path = require('path');

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'src/frontend/styles')],
  },
  webpack: (config) => {
    return config;
  },
  redirects: async () => {
    return [
      {
        source: '/',
        destination: '/stands',
        permanent: true,
      },
    ]
  }
}

module.exports = nextConfig
