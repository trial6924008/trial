FROM node:16-alpine
WORKDIR /usr/src/app
ENV DATABASE_URL=mongodb://root:pass@mongodb:27017/trial?authSource=admin&directConnection=true
COPY package*.json ./
COPY prisma prisma
COPY public public
COPY dist dist
COPY .next .next
COPY .env ./
RUN npm ci --ignore-scripts --ignore-engines
RUN npm run db:generate
EXPOSE 3000
EXPOSE 27017
CMD [ "node", "dist/main.js" ]