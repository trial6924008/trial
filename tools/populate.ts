import { NestFactory } from '@nestjs/core';
import { AppModule } from '../src/app.module';
import { ProductsService } from '../src/products/services/products.service';
import { IProduct } from '../src/shared/products/IProduct';
import { IStand } from '../src/shared/stands/IStand';
import { ITag } from '../src/shared/tags/ITag';
import { StandsService } from '../src/stands/services/stands.service';
import { TagsService } from '../src/tags/services/tags.service';

async function populate() {
    const numberOfStands = 10;
    const productsNames = [
        'bread',
        'oil',
        'eggs',
        'cheese',
        'fish'
    ];
    const productsPrices = [150, 250, 180, 400, 500];

    const stands: IStand[] = [];
    const products: IProduct[] = [];
    const tags: ITag[] = [];

    const app = await NestFactory.create(AppModule);

    const standsService = app.get(StandsService);
    const productsService = app.get(ProductsService);
    const tagsService = app.get(TagsService);

    for (let i = 1; i <= numberOfStands; i++) {
        const address = `${i} Test St.`;
        const contacts = `+1 (484) ${i.toString(10).padStart(6, '0')}`;
        const name = `Test stand №${i}`;
        try {
            const stand = await standsService.createStand({
                address,
                contacts,
                name,
            });
            stands.push(stand);
            if (i % 2) {
                await standsService.editStand({
                    id: stand.id,
                    online: true,
                })
            }
        } catch (e) {
            console.error(e);
        }
    }

    for (let i = 0; i < productsNames.length; i++) {
        const name = productsNames[i];
        const price = productsPrices[i];
        try {
            const product = await productsService.createProduct({
                name,
                price,
            });
            products.push(product);
        } catch (e) {
            console.error(e);
        }
    }

    for (let stand of stands) {
        const standId = stand.id;
        for (let product of products) {
            const productId = product.id;
            const numberOfTags = Math.floor(Math.random() * 10) + 1;
            for (let i = 0; i < numberOfTags; i++) {
                try {
                    const tag = await tagsService.createTag({
                        standId,
                        productId,
                    });
                    tags.push(tag);
                } catch (e) {
                    console.error(e);
                }
            }
        }
    }

    let soldIndexes: number[] = []
    const soldNumber = Math.floor(Math.random() * tags.length) + 1;

    for (let i = 0; i < soldNumber; i++) {
        const index = Math.floor(Math.random() * tags.length-1) + 1;
        soldIndexes.push(index);
    }

    soldIndexes = soldIndexes.filter((v, i, arr) => arr.indexOf(v) === i);
    
    for (let index of soldIndexes) {
        try {
            await tagsService.editTag({
                id: tags[index].id,
                solded: true,
            });
        } catch (e) {
            console.error(e);
        }
    }

    await app.close();
}
populate();
