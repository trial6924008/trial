import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { standsSelector } from '../../features/stands/stands.selector';
import { IColumn } from '../../core/tables/types/IColumn';
import { IProps } from './sold-products-list.interfaces';
import { IRow } from '../../core/tables/types/IRow';
import { StandProductsTable } from '../../core/tables/stand-products.table';
import { productsSelector } from '../../features/products/products.selector';
import { tagsSelector } from '../../features/tags/tags.selector';
import { IProduct } from '../../../shared/products/IProduct';

type productsMap = {
    [key: string]: Omit<IProduct, 'id'>;
}

export function SoldProductsListController(): IProps {
    const router = useRouter();
    const { standId } = router.query;
    const { standsList } = useSelector(standsSelector);
    const { loading: productsLoading, productsList } = useSelector(productsSelector);
    const { loading: tagsLoading, tagsList } = useSelector(tagsSelector);
    const loading = useMemo(
        () => productsLoading || tagsLoading, 
        [productsLoading, tagsLoading]
    );
    const standName = useMemo(
        () => standsList
            .filter(({ id }) => id === standId)
            .map(({ name }) => name)
            .shift() ?? '', 
        [standsList, standId]
    )
    const productsMap = useMemo(
        () => productsList.reduce((acc, product) => {
            const _val = { ...product } as Partial<IProduct>;
            delete _val.id;
            return {
                ...acc,
                [product.id]: _val as Omit<IProduct, 'id'>
            }
        }, {} as productsMap), 
        [productsList]
    );
    const { tags, productsIdList } = useMemo(
        () => {
            const productsIdList: string[] = [];
            const tags = tagsList
                .filter(
                    tag => {
                        const isValid = tag.standId === standId && tag.solded && !!productsMap[tag.productId];
                        if (isValid && !productsIdList.includes(tag.productId)) {
                            productsIdList.push(tag.productId)
                        }
                        return isValid;
                    }
                );
            return { tags, productsIdList };
        },
        [tagsList, productsMap, standId]
    );
    const products = useMemo(
        () => productsList
            .filter(({ id }) => productsIdList.includes(id))
            .map(({ id, name, price }) => ({
                name,
                price,
                number: tags.filter(tag => tag.productId === id).length
            })),
        [productsList, productsIdList, tags]
    );
    const columns: IColumn[] = useMemo(
        () => [
            { field: 'name', headerName: 'Name' },
            { field: 'price', headerName: 'Price' },
            { field: 'number', headerName: 'Number' },
        ],
        [],
    );
    const rows: IRow[] = useMemo(() => {
        return new StandProductsTable({
            columns,
            productsList: products,
        }).rows
    }, [columns, products]);

    return {
        standName,
        tableProps: {
            columns,
            rows,
            loading,
        },
    };
}
