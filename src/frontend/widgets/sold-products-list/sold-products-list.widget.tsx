import React from 'react';
import { SoldProductsListController } from './sold-products-list.controller';
import { SoldProductsListView } from './sold-products-list.view';

const Container: React.FC = () => {
    const props = SoldProductsListController();
    return <SoldProductsListView {...props} />;
};

export const SoldProductsListWidget = React.memo(Container);
