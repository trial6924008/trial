import React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { Table } from '../../ui/table/table';
import { IProps } from './sold-products-list.interfaces';
import styles from './sold-products-list.module.scss';

const View: React.FC<IProps> = ({ standName, tableProps }: IProps) => {
    return (
        <Box className={styles['sold-products-list__wrap']} component="section">
            <Paper className={styles['sold-products-list']}>
                <Typography className={styles['sold-products-list__title']} variant="h2">
                    {`Sold products of stand${standName ? `: "${standName}"`: ''}`}
                </Typography>
                <Table {...tableProps} />
            </Paper>
        </Box>
    );
};

export const SoldProductsListView = React.memo(View);
