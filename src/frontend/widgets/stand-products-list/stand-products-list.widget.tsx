import React from 'react';
import { StandProductsListController } from './stand-products-list.controller';
import { StandProductsListView } from './stand-products-list.view';

const Container: React.FC = () => {
    const props = StandProductsListController();
    return <StandProductsListView {...props} />;
};

export const StandProductsListWidget = React.memo(Container);
