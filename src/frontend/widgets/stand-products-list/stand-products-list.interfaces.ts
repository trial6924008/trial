import { ITableProps } from '../../ui/table/table.interfaces';

export interface IProps {
    standName: string;
    tableProps: ITableProps;
}
