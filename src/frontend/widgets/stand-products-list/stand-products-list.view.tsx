import React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { Table } from '../../ui/table/table';
import { IProps } from './stand-products-list.interfaces';
import styles from './stand-products-list.module.scss';

const View: React.FC<IProps> = ({ standName, tableProps }: IProps) => {
    return (
        <Box className={styles['stand-products-list__wrap']} component="section">
            <Paper className={styles['stand-products-list']}>
                <Typography className={styles['stand-products-list__title']} variant="h2">
                    {`Products on stand${standName ? `: "${standName}"`: ''}`}
                </Typography>
                <Table {...tableProps} />
            </Paper>
        </Box>
    );
};

export const StandProductsListView = React.memo(View);
