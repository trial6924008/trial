import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { notificationsSelector } from '../../features/notifications/notifications.selector';
import { IProps } from './notifications-bar.interfaces';

export function NotificationsBarController(): IProps {
    const notificationsState = useSelector(notificationsSelector);
    const { enqueueSnackbar } = useSnackbar();

    const [lastId, setLastId] = useState('');

    useEffect(() => {
        const messages = notificationsState.messages;
        const length = messages.length;
        if (length) {
            const lastMessage = messages[length - 1];
            const { payload, type, id } = lastMessage;
            if (lastId !== id) {
                enqueueSnackbar(payload, { variant: type, key: id });
                setLastId(id);
            }
        }
    }, [notificationsState.messages.length]);

    return {};
}
