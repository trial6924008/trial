import React from 'react';
import { IProps } from './notifications-bar.interfaces';

const View: React.FC<IProps> = ({}: IProps) => {
    return <></>;
};

export const NotificationsBarView = React.memo(View);
