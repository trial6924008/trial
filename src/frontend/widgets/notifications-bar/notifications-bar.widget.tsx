import React from 'react';
import { NotificationsBarController } from './notifications-bar.controller';
import { NotificationsBarView } from './notifications-bar.view';

const Container: React.FC = () => {
    const props = NotificationsBarController();
    return <NotificationsBarView {...props} />;
};

export const NotificationsBarWidget = React.memo(Container);
