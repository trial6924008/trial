import React from 'react';
import { StandsListController } from './stands-list.controller';
import { StandsListView } from './stands-list.view';

const Container: React.FC = () => {
    const props = StandsListController();
    return <StandsListView {...props} />;
};

export const StandsListWidget = React.memo(Container);
