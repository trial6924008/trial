import React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { Table } from '../../ui/table/table';
import { IProps } from './stands-list.interfaces';
import styles from './stands-list.module.scss';

const View: React.FC<IProps> = ({ tableProps }: IProps) => {
    return (
        <Box className={styles['stands-list__wrap']} component="section">
            <Paper className={styles['stands-list']}>
                <Typography className={styles['stands-list__title']} variant="h2">
                    Stands
                </Typography>
                <Table {...tableProps} />
            </Paper>
        </Box>
    );
};

export const StandsListView = React.memo(View);
