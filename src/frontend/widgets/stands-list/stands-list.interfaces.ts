import { ITableProps } from '../../ui/table/table.interfaces';

export interface IProps {
    tableProps: ITableProps;
}
