import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import MenuIcon from '@mui/icons-material/Menu';
import { standsSelector } from '../../features/stands/stands.selector';
import { IColumn } from '../../core/tables/types/IColumn';
import { IProps } from './stands-list.interfaces';
import { IRow } from '../../core/tables/types/IRow';
import { StandsTable } from '../../core/tables/stands.table';
import { IMenuItem } from '../../ui/menu/menu.interfaces';
import { Menu } from '../../ui/menu/menu';

type mapObject = {
    [key: string]: string;
}

export function StandsListController(): IProps {
    const { loading, standsList } = useSelector(standsSelector);
    const columns: IColumn[] = useMemo(
        () => [
            { field: 'name', headerName: 'Name' },
            { field: 'online', headerName: 'Status of stand' },
            { field: 'tagsCount', headerName: 'Number of tags on the stand' },
            { field: 'additionalInfo', headerName: 'Additional info' },
        ],
        [],
    );
    const standsMap = useMemo(() => {
        return standsList.reduce((acc, val) => ({
            ...acc,
            [val.name]: val.id
        }), {} as mapObject)
    }, [standsList])
    const rows: IRow[] = useMemo(() => {
        return new StandsTable({
            columns,
            standsList,
        }).rows.map((row) => {
            const items: IMenuItem[] = [
                {
                    text: 'Stand info',
                    link: `/stands/${standsMap[row.name as string]}`,
                },
            ];
            const btnIcon = <MenuIcon />;
            const menu = <Menu items={items} button={btnIcon} />;
            const additionalInfo: IRow = {
                additionalInfo: menu,
            };
            return { ...row, ...additionalInfo };
        });
    }, [columns, standsList]);

    return {
        tableProps: {
            columns,
            rows,
            loading,
        },
    };
}
