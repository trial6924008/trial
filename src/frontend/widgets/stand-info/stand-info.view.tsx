import React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { IProps } from './stand-info.interfaces';
import styles from './stand-info.module.scss';
import { listIcons } from '../../ui/list/types/list-icons';
import { List } from '../../ui/list/list';
import { IItemProps } from '../../ui/list/types/IItemProps';

const View: React.FC<IProps> = ({
    loading,
    address,
    contacts,
    sellProducts,
    soldProducts,
    standName,
}: IProps) => {
    const items: IItemProps[] = [
        {
            title: 'Address',
            label: address,
            icon: listIcons.address,
        },
        {
            title: 'Contacts',
            label: contacts,
            icon: listIcons.contacts,
        },
        {
            title: 'Tags on stand',
            label: sellProducts,
            icon: listIcons.sell,
        },
        {
            title: 'Sold tags',
            label: soldProducts,
            icon: listIcons.sold,
        },
    ];

    return (
        <Box className={styles['stand-info__wrap']} component="section">
            <Paper className={styles['stand-info']}>
                <Typography className={styles['stand-info__title']} variant="h2">
                    {`Stand${standName ? ` – ${standName}` : ''}`}
                </Typography>
                <List items={items} loading={loading} />
            </Paper>
        </Box>
    );
};

export const StandInfoView = React.memo(View);
