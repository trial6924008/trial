import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { standsSelector } from '../../features/stands/stands.selector';
import { IProps } from './stand-info.interfaces';
import { tagsSelector } from '../../features/tags/tags.selector';

export function StandInfoController(): IProps {
    const router = useRouter();
    const { standId } = router.query;
    const { loading: standsLoading, standsList } = useSelector(standsSelector);
    const { loading: tagsLoading, tagsList } = useSelector(tagsSelector);

    const loading = useMemo(() => standsLoading || tagsLoading, [standsLoading, tagsLoading]);
    const stand = useMemo(() => standsList.filter(({ id }) => id === standId).shift(), [standsList]);
    const standName = useMemo(
        () => standsList
            .filter(({ id }) => id === standId)
            .map(({ name }) => name)
            .shift() ?? '', 
        [standsList, standId]
    );
    const address = useMemo(() => stand?.address ?? 'no data', [stand]);
    const contacts = useMemo(() => stand?.contacts ?? 'no data', [stand]);
    const { sellProducts, soldProducts } = useMemo(
        () => {
            let sell = 0;
            let sold = 0;
            tagsList.forEach(({ standId: id, solded }) => {
                if (standId === id) {
                    solded ? sold++ : sell++;
                }
            });
            return {
                sellProducts: String(sell),
                soldProducts: String(sold),
            };
        },
        [standId, tagsList]
    );

    return {
        loading,
        address,
        contacts,
        sellProducts,
        soldProducts,
        standName,
    };
}
