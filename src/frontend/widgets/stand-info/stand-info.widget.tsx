import React from 'react';
import { StandInfoController } from './stand-info.controller';
import { StandInfoView } from './stand-info.view';

const Container: React.FC = () => {
    const props = StandInfoController();
    return <StandInfoView {...props} />;
};

export const StandInfoWidget = React.memo(Container);
