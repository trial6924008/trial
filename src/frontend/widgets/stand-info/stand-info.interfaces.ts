export interface IProps {
    loading: boolean;
    address: string;
    contacts: string;
    sellProducts: string;
    soldProducts: string;
    standName: string;
}
