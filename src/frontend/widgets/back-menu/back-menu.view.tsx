import React from 'react';
import Box from '@mui/material/Box';
import { IProps } from './back-menu.interfaces';
import { Button } from '../../ui/button/button';
import { buttinIcons } from '../../ui/button/button.interfaces';
import styles from './back-menu.module.scss';

const View: React.FC<IProps> = ({ handleBackClick }: IProps) => {
    return (
        <Box className={styles['back-menu__wrap']} component="section">
            <Button 
                title="Back to stands"
                variant="text"
                icon={buttinIcons.back}
                onClick={handleBackClick}
            />
        </Box>
    );
};

export const BackMenuView = React.memo(View);
