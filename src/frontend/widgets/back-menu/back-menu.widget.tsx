import React from 'react';
import { BackMenuController } from './back-menu.controller';
import { BackMenuView } from './back-menu.view';

const Container: React.FC = () => {
    const props = BackMenuController();
    return <BackMenuView {...props} />;
};

export const BackMenuWidget = React.memo(Container);
