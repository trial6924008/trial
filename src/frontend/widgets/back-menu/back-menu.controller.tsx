import { useCallback } from 'react';
import { useRouter } from 'next/router';
import { IProps } from './back-menu.interfaces';

export function BackMenuController(): IProps {
    const { push } = useRouter();

    const handleBackClick = useCallback(() => {
        push('/stands');
    }, []);

    return {
        handleBackClick
    };
}
