import { useState, useCallback } from 'react';
import {
    ITableController,
    pageChangeHandler,
    rowsPerPageChangeHandler,
} from './table.interfaces';

export function TableController(): ITableController {
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const onPageChange = useCallback<pageChangeHandler>(
        (e, page) => setPage(page),
        [],
    );

    const onRowsPerPageChange = useCallback<rowsPerPageChangeHandler>((e) => {
        const value = parseInt(e.target.value);
        setPage(page);
        setRowsPerPage(value);
    }, []);

    return {
        page,
        rowsPerPage,
        onPageChange,
        onRowsPerPageChange,
    };
}
