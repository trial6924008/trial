import React from 'react';
import TableBase from '@mui/material/Table';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import { TableHeader } from './components/table-header';
import { TableData } from './components/table-data';
import { TableFooter } from './components/table-footer';
import { ITableProps } from './table.interfaces';
import { TableController } from './table.controller';
import styles from './table.module.scss';

const Component: React.FC<ITableProps> = ({
    columns,
    rows,
    loading,
}: ITableProps) => {
    const { page, rowsPerPage, onPageChange, onRowsPerPageChange } =
        TableController();
    const rowsNumber = rows.length;
    return (
        <Paper className={styles['table__wrap']}>
            <TableContainer className={styles['table__container']}>
                <TableBase className={styles['table']} size="small">
                    <TableHeader columns={columns} />
                    <TableData
                        columns={columns}
                        rows={rows}
                        loading={loading}
                        page={page}
                        rowsPerPage={rowsPerPage}
                    />
                </TableBase>
            </TableContainer>
            <TableFooter
                rowsNumber={rowsNumber}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={onPageChange}
                onRowsPerPageChange={onRowsPerPageChange}
            />
        </Paper>
    );
};

export const Table = React.memo(Component);
