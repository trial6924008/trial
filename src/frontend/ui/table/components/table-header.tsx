import React from 'react';
import TableHead from '@mui/material/TableHead';
import { HeaderRow } from './header-row';
import { ITableProps } from '../table.interfaces';
import styles from '../table.module.scss';

type IProps = Pick<ITableProps, 'columns'>;

const Component: React.FC<IProps> = ({ columns }: IProps) => {
    return (
        <TableHead className={styles['table__header']}>
            <HeaderRow columns={columns} />
        </TableHead>
    );
};

export const TableHeader = React.memo(Component);
