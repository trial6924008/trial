import React from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import styles from '../table.module.scss';

interface IProps {
    columnsNumber: number;
}

const Component: React.FC<IProps> = ({ columnsNumber }: IProps) => {
    return (
        <TableRow className={styles['table__row']}>
            <TableCell
                className={styles['table__placeholder']}
                colSpan={columnsNumber}
            >
                <Typography
                    className={styles['table__placeholder-text']}
                    variant="body1"
                >
                    No data
                </Typography>
            </TableCell>
        </TableRow>
    );
};

export const EmptyPlaceholder = React.memo(Component);
