import React from 'react';
import TableBody from '@mui/material/TableBody';
import { BodyRows } from './body-rows';
import { EmptyPlaceholder } from './empty-placeholder';
import { LoadingPlaceholder } from './loading-placeholder';
import { ITableProps, ITableController } from '../table.interfaces';
import styles from '../table.module.scss';

interface IProps
    extends ITableProps,
        Pick<ITableController, 'page' | 'rowsPerPage'> {}

const Component: React.FC<IProps> = ({
    columns,
    rows,
    loading,
    page,
    rowsPerPage,
}: IProps) => {
    let content: React.ReactElement;
    if (loading) {
        content = <LoadingPlaceholder columnsNumber={columns.length} />;
    } else if (!rows.length) {
        content = <EmptyPlaceholder columnsNumber={columns.length} />;
    } else {
        content = (
            <BodyRows
                columns={columns}
                rows={rows}
                page={page}
                rowsPerPage={rowsPerPage}
            />
        );
    }
    return <TableBody className={styles['table__body']}>{content}</TableBody>;
};

export const TableData = React.memo(Component);
