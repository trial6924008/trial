import React from 'react';
import TablePagination from '@mui/material/TablePagination';
import { ITableController } from '../table.interfaces';
import styles from '../table.module.scss';

interface IProps
    extends Pick<
        ITableController,
        'page' | 'rowsPerPage' | 'onPageChange' | 'onRowsPerPageChange'
    > {
    rowsNumber: number;
}

const Component: React.FC<IProps> = ({
    rowsNumber,
    page,
    rowsPerPage,
    onPageChange,
    onRowsPerPageChange,
}: IProps) => {
    return (
        <>
            <TablePagination
                classes={{
                    root: styles['table__footer'],
                    toolbar: styles['table__footer-toolbar'],
                    spacer: styles['table__footer-spacer'],
                    selectLabel: styles['table__footer-select-label'],
                    select: styles['table__footer-select'],
                    selectIcon: styles['table__footer-select-icon'],
                    input: styles['table__footer-select-input'],
                    menuItem: styles['table__footer-select-menu-item'],
                    displayedRows: styles['table__footer-displayed-rows'],
                }}
                component="div"
                count={rowsNumber}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={onPageChange}
                onRowsPerPageChange={onRowsPerPageChange}
            />
        </>
    );
};

export const TableFooter = React.memo(Component);
