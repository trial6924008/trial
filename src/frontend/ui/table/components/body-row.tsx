import React from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import { ITableProps } from '../table.interfaces';
import { IRow } from '../../../core/tables/types/IRow';
import styles from '../table.module.scss';

interface IProps extends Pick<ITableProps, 'columns'> {
    row: IRow;
}

const Component: React.FC<IProps> = ({ columns, row }: IProps) => {
    return (
        <TableRow className={styles['table__row']}>
            {columns.map(({ field }, i) => (
                <TableCell
                    className={styles['table__cell']}
                    key={`${field}${i}`}
                >
                    <Typography className={styles['table__cell-text']}>
                        {row[field] ?? '—'}
                    </Typography>
                </TableCell>
            ))}
        </TableRow>
    );
};

export const BodyRow = React.memo(Component);
