import React from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import { ITableProps } from '../table.interfaces';
import styles from '../table.module.scss';

type IProps = Pick<ITableProps, 'columns'>;

const Component: React.FC<IProps> = ({ columns }: IProps) => {
    return (
        <TableRow className={styles['table__row']}>
            {columns.map(({ headerName }, i) => (
                <TableCell
                    className={styles['table__cell']}
                    key={`${headerName}${i}`}
                >
                    <Typography
                        className={styles['table__cell-text_header']}
                        fontWeight="bold"
                    >
                        {headerName}
                    </Typography>
                </TableCell>
            ))}
        </TableRow>
    );
};

export const HeaderRow = React.memo(Component);
