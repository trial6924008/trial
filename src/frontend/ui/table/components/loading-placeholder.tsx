import React from 'react';
import Container from '@mui/material/Container';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import CircularProgress from '@mui/material/CircularProgress';
import styles from '../table.module.scss';

interface IProps {
    columnsNumber: number;
}

const Component: React.FC<IProps> = ({ columnsNumber }: IProps) => {
    return (
        <TableRow className={styles['table__row']}>
            <TableCell
                className={styles['table__placeholder']}
                colSpan={columnsNumber}
            >
                <Container className={styles['table__placeholder-loader-wrap']}>
                    <CircularProgress
                        className={styles['table__placeholder-loader']}
                        size="3rem"
                    />
                </Container>
            </TableCell>
        </TableRow>
    );
};

export const LoadingPlaceholder = React.memo(Component);
