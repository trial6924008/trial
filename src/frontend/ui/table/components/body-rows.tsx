import React from 'react';
import { BodyRow } from './body-row';
import { ITableProps, ITableController } from '../table.interfaces';

interface IProps
    extends Pick<ITableProps, 'columns' | 'rows'>,
        Pick<ITableController, 'page' | 'rowsPerPage'> {}

const Component: React.FC<IProps> = ({
    columns,
    rows,
    page,
    rowsPerPage,
}: IProps) => {
    return (
        <>
            {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, i) => (
                    <BodyRow columns={columns} row={row} key={i} />
                ))}
        </>
    );
};

export const BodyRows = React.memo(Component);
