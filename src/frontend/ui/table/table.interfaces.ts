import React from 'react';
import { IColumn } from '../../core/tables/types/IColumn';
import { IRow } from '../../core/tables/types/IRow';

export interface ITableController {
    page: number;
    rowsPerPage: number;
    onPageChange: (
        e: React.MouseEvent<HTMLButtonElement> | null,
        page: number,
    ) => void;
    onRowsPerPageChange: React.ChangeEventHandler<
        HTMLInputElement | HTMLTextAreaElement
    >;
}

export type pageChangeHandler = NonNullable<ITableController['onPageChange']>;

export type rowsPerPageChangeHandler = NonNullable<
    ITableController['onRowsPerPageChange']
>;

export interface ITableProps {
    columns: IColumn[];
    rows: IRow[];
    loading?: boolean;
}
