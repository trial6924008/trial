import React from 'react';

export interface IMenuItem {
    text: string;
    link?: string;
    onClick?: React.MouseEventHandler<HTMLElement>;
}

export interface IMenuController {
    anchor: HTMLButtonElement | null;
    onOpen: React.MouseEventHandler<HTMLButtonElement>;
    onClose: React.MouseEventHandler<HTMLButtonElement>;
}

export interface IMenuProps {
    items: IMenuItem[];
    button: string | React.ReactElement;
}
