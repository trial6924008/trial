import React from 'react';
import Link from 'next/link';
import MuiMenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import { IMenuItem } from '../menu.interfaces';
import styles from '../menu.module.scss';

interface IProps extends IMenuItem {}

const Component: React.FC<IProps> = ({ text, link, onClick }: IProps) => {
    const label = (
        <Typography className={styles['menu__item-text']} variant="body1">
            {text}
        </Typography>
    );

    const item = (
        <MuiMenuItem
            className={styles['menu__item']}
            id={text}
            onClick={link ? undefined : onClick}
        >
            {label}
        </MuiMenuItem>
    );

    if (link) {
        return (
            <Link className={styles['menu__item-link']} href={link}>
                {item}
            </Link>
        );
    } else {
        return <>{item}</>;
    }
};

export const MenuItem = React.memo(Component);
