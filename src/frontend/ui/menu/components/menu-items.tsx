import React from 'react';
import MenuList from '@mui/material/Menu';
import { MenuItem } from './menu-item';
import { IMenuProps, IMenuController } from '../menu.interfaces';
import styles from '../menu.module.scss';

interface IProps
    extends Pick<IMenuProps, 'items'>,
        Omit<IMenuController, 'onOpen'> {}

const Component: React.FC<IProps> = ({ items, anchor, onClose }: IProps) => {
    return (
        <MenuList
            className={styles['menu__list']}
            open={Boolean(anchor)}
            anchorEl={anchor}
            onClose={onClose}
        >
            {items.map(({ text, link, onClick }, i) => (
                <MenuItem
                    text={text}
                    link={link}
                    onClick={onClick}
                    key={`${text}${i}`}
                />
            ))}
        </MenuList>
    );
};

export const MenuItems = React.memo(Component);
