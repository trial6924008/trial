import React from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { IMenuController, IMenuProps } from '../menu.interfaces';
import styles from '../menu.module.scss';

interface IProps extends Pick<IMenuProps, 'button'> {
    onClick: IMenuController['onOpen'];
}

const Component: React.FC<IProps> = ({ button, onClick }: IProps) => {
    return (
        <Button
            className={styles['menu__btn']}
            variant="text"
            onClick={onClick}
        >
            {typeof button === 'string' ? (
                <Typography
                    className={styles['menu__btn-text']}
                    variant="body1"
                >
                    {button}
                </Typography>
            ) : (
                <>{button}</>
            )}
        </Button>
    );
};

export const MenuButton = React.memo(Component);
