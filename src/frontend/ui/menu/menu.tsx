import React from 'react';
import { MenuButton } from './components/menu-button';
import { MenuItems } from './components/menu-items';
import { MenuController } from './menu.controller';
import { IMenuProps } from './menu.interfaces';

const Component: React.FC<IMenuProps> = ({ button, items }: IMenuProps) => {
    const { anchor, onOpen, onClose } = MenuController(items);
    return (
        <>
            <MenuButton button={button} onClick={onOpen} />
            <MenuItems items={items} anchor={anchor} onClose={onClose} />
        </>
    );
};

export const Menu = React.memo(Component);
