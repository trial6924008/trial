import { useState, useCallback } from 'react';
import { IMenuProps, IMenuController } from './menu.interfaces';

export function MenuController(items: IMenuProps['items']): IMenuController {
    const itemsLength = items.length;
    const [anchor, setAnchor] = useState<HTMLButtonElement | null>(null);

    const onOpen: React.MouseEventHandler<HTMLButtonElement> = useCallback(
        (e) => {
            const elemet = e.currentTarget;
            setAnchor(elemet);
            if (itemsLength) setAnchor(elemet);
        },
        [itemsLength],
    );

    const onClose: React.MouseEventHandler<HTMLButtonElement> =
        useCallback(() => {
            setAnchor(null);
        }, []);

    return {
        anchor,
        onOpen,
        onClose,
    };
}
