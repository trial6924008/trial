import { buttinIcon } from '../button.interfaces';

export interface IButtonIconProps {
    icon?: buttinIcon;
}