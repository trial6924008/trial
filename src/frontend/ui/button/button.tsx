import React from 'react';
import MuiButton, { ButtonProps } from '@mui/material/Button';
import { IButtonProps } from './button.interfaces';
import { ButtonIcon } from './components/button-icon';
import styles from './button.module.scss';

const Component: React.FC<IButtonProps> = ({
    title,
    icon,
    disabled,
    type,
    variant,
    onClick,
}: IButtonProps) => {
    return (
        <MuiButton
            className={styles['button']}
            variant={variant ?? 'contained'}
            disabled={disabled}
            id={title}
            type={type}
            onClick={onClick}
        >
            <ButtonIcon icon={icon} />
            {title}
        </MuiButton>
    );
};

export const Button = React.memo(Component);
