import React from 'react';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import { buttinIcons } from '../button.interfaces';
import { IButtonIconProps } from '../types/IButtonIconProps';

const Component: React.FC<IButtonIconProps> = ({ icon }: IButtonIconProps) => {
    switch (icon) {
        case buttinIcons.back:
            return (
                <>
                    <ArrowBackIosNewIcon />
                </>
            );
        default:
            return <></>;
    }
}

export const ButtonIcon = React.memo(Component);
