import { ButtonProps } from '@mui/material/Button';

export enum buttinIcons {
    back = 'back',
}

export type buttinIcon = `${buttinIcons}`;

export interface IButtonProps {
    title: string;
    icon?: buttinIcon;
    disabled?: boolean;
    type?: ButtonProps['type'];
    variant?: ButtonProps['variant'];
    onClick?: ButtonProps['onClick'];
}
