import React from 'react';
import Box from '@mui/material/Box';
import ListMui from '@mui/material/List';
import CircularProgress from '@mui/material/CircularProgress';
import { IListProps } from './types/IListProps';
import { Item } from './components/item';
import styles from './list.module.scss';

const Component: React.FC<IListProps> = ({ items, loading }: IListProps) => {
    if (loading) {
        return (
            <Box className={styles['list__loader-wrap']}>
                <CircularProgress />
            </Box>
        );
    } else {
        return (
            <ListMui>
                {
                    items.map(props => <Item {...props} key={props.title} />)
                }
            </ListMui>
        );
    }
}

export const List = React.memo(Component);
