import { IIconProps } from './IIconProps';

export interface IItemProps extends IIconProps {
    title: string;
    label: string;
}
