import { IItemProps } from './IItemProps';

export interface IListProps {
    loading: boolean;
    items: IItemProps[];
}
