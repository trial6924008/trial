import { listIcon } from './list-icon';

export interface IIconProps {
    icon?: listIcon;
}
