import React from 'react';
import ImageIcon from '@mui/icons-material/Image';
import BusinessIcon from '@mui/icons-material/Business';
import ContactPageIcon from '@mui/icons-material/ContactPage';
import StorefrontIcon from '@mui/icons-material/Storefront';
import SellIcon from '@mui/icons-material/Sell';
import { IIconProps } from '../types/IIconProps';
import { listIcons } from '../types/list-icons';

const Component: React.FC<IIconProps> = ({ icon }: IIconProps) => {
    switch (icon) {
        case listIcons.address:
            return <BusinessIcon />
        case listIcons.contacts:
            return <ContactPageIcon />
        case listIcons.sell:
            return <StorefrontIcon />
        case listIcons.sold:
            return <SellIcon />
        default:
            return <ImageIcon />
    }
}

export const Icon = React.memo(Component);
