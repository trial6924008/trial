import React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import { IItemProps } from '../types/IItemProps';
import { Icon } from './icon';

const Component: React.FC<IItemProps> = ({ title, label, icon }: IItemProps) => {
    return (
        <ListItem>
            <ListItemAvatar>
                <Avatar>
                    <Icon icon={icon} />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={title} secondary={label} />
        </ListItem>
    );
}

export const Item = React.memo(Component);
