import { apiMethod } from './api-method';

export interface IRequestOptions {
    pathname: string;
    method: apiMethod;
    body?: object;
}