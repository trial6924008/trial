export enum apiMethods {
    get = 'get',
    post = 'post',
    put = 'put',
    delete = 'delete',
}
