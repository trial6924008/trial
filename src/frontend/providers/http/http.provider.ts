import { Inject, Service } from 'typedi';
import { ApiConfig } from '../../config/api.config';
import { defaultPorts } from './default-ports';
import { IRequestOptions } from './IRequestOptions';
import { protocol } from './protocol';
import { protocols } from './protocols';

@Service()
export class HttpProvider {
    private readonly path: string;

    constructor(
        @Inject() private readonly config: ApiConfig
    ) {
        this.path = this.getPath();
    }
    
    private getPortStr(port: number, protocol: protocol): string {
        const isHttpDefault =
            protocol === protocols.http && port === defaultPorts.http;
        const isHttpsDefault =
            protocol === protocols.https && port === defaultPorts.https;
        return isHttpDefault || isHttpsDefault ? '' : `:${port}`;
    }

    private getPath(): string {
        const { host, port, encryption } = this.config;
        const protocol: protocol = encryption
            ? protocols.https
            : protocols.http;
        const portStr = this.getPortStr(port, protocol);
        return `${protocol}://${host}${portStr}`;
    }

    private getPathPart(pathpart: string): string {
        let result = pathpart;
        if (!pathpart) return pathpart;
        const regExp = /^\/.*$/;
        const isValid = regExp.test(pathpart);
        if (!isValid) result = `/${pathpart}`;
        return result;
    }

    private getUrl(pathname: string): string {
        return `${this.path}${this.getPathPart(pathname)}`;
    }

    public async request({
        pathname,
        method,
        body
    }: IRequestOptions): Promise<any> {
        try {
            const url = this.getUrl(pathname);
            const res = await fetch(url, {
                method,
                headers: { 'Content-Type': 'application/json' },
                body: body ? JSON.stringify(body) : null,
            });
            if (res.ok) {
                return res.json();
            }
        } catch (e) {

        }
    }
}
