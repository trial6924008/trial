import { apiMethods } from './api-methods';

export type apiMethod = `${apiMethods}`;
