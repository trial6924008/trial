import { Theme } from '@mui/material/styles';

export abstract class AbstractTheme {
    protected abstract readonly defaultFontSize: number;

    protected getRemFontSize(size: number): string {
        return `${size / this.defaultFontSize}rem`;
    }

    protected abstract createTheme(): Theme;

    public get theme(): Theme {
        return this.createTheme();
    }
}
