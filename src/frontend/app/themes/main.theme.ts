import { createTheme } from '@mui/material/styles';
import { Service } from 'typedi';
import variables from '../../styles/var.module.scss';
import { AbstractTheme } from './abstract.theme';

@Service()
export class MainTheme extends AbstractTheme {
    protected override readonly defaultFontSize = 16;

    protected createTheme() {
        const fontSize = parseInt(variables.baseFontSize);
        const h2BaseFontSize = parseInt(variables.sectionFontSize);
        const h3BaseFontSize = parseInt(variables.headerFontSize);
        const h2FontSize = this.getRemFontSize(h2BaseFontSize);
        const h3FontSize = this.getRemFontSize(h3BaseFontSize);
        const bodyFontSize = this.getRemFontSize(fontSize);

        return createTheme({
            palette: {
                primary: {
                    main: variables.primary1,
                    light: variables.primary2,
                    dark: variables.primary2,
                },
                error: {
                    main: variables.error1,
                    light: variables.error2,
                    dark: variables.error2,
                },
            },
            typography: {
                fontSize: fontSize,
                h2: {
                    fontSize: h2FontSize,
                },
                h3: {
                    fontSize: h3FontSize,
                },
                body1: {
                    fontSize: bodyFontSize,
                },
                body2: {
                    fontSize: bodyFontSize,
                },
            },
        });
    }
}

