import { useSelector } from 'react-redux';
import { appSelector } from '../../features/app/app.selector';

export function useCursor() {
    const appState = useSelector(appSelector);

    let cursor = '';
    if (appState.loading) cursor = 'progress';
    return cursor;
}
