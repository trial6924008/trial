import { ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { Service } from 'typedi';
import { AbstractReducer } from '../../store/abstract.reducer';
import { INotificationsState } from './INotificationsState';
import { NotificationsActions } from './notifications.actions';

@Service()
export class NotificationsReducer extends AbstractReducer<INotificationsState> {
    protected override initialState = {
        messages: [],
    };

    protected override buildReducer(
        builder: ActionReducerMapBuilder<INotificationsState>,
    ): void {
        builder
            .addCase(NotificationsActions.addMessage, (state, action) => {
                const notification = action.payload;
                state.messages.push(notification);
            })
            .addCase(NotificationsActions.removeMessage, (state, action) => {
                state.messages = state.messages.filter(
                    (message) => message.id !== action.payload,
                );
            })
            .addCase(NotificationsActions.clearMessages, (state, action) => {
                state.messages = [];
            });
    }
}
