import { createSelector } from '@reduxjs/toolkit';
import { selector } from '../../store/selector';
import { IRootState } from '../../store/IRootState';
import { INotificationsState } from './INotificationsState';

export const notificationsSelector: selector<INotificationsState> =
    createSelector(
        (state: IRootState) => state,
        (state: IRootState) => state.notifications,
    );
