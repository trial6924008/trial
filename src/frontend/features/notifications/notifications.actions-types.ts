export enum notificationsActionsTypes {
    addError = 'addError',
    addSuccess = 'addSuccess',
    addMessage = 'addMessage',
    removeMessage = 'removeMessage',
    clearMessages = 'clearMessages',
}
