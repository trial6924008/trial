import { v4 as uuid } from 'uuid';
import { INotification } from './types/INotification';
import { notificationsType } from './types/notificationsType';

export class Notification implements INotification {
    public id: string;

    constructor(
        public readonly type: notificationsType,
        public readonly payload: string,
    ) {
        this.id = uuid();
    }
}
