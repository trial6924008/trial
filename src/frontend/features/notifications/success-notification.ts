import { Notification } from './notification';
import { notificationsTypes } from './enums/notifications-types';
import { INotification } from './types/INotification';
import { successNotifications } from './enums/success-notifications';

export class SuccessNotification extends Notification implements INotification {
    constructor(payload: successNotifications) {
        super(notificationsTypes.success, payload);
    }
}
