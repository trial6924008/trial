import { PayloadAction } from '@reduxjs/toolkit';
import { ofType } from 'redux-observable';
import type { Epic as epic } from 'redux-observable';
import { Service } from 'typedi';
import { delay, map } from 'rxjs/operators';
import { Effect } from '../../store/effect.decorator';
import { AbstractEffects } from '../../store/abstract.effects';
import { NotificationsActions } from './notifications.actions';
import { notificationsActionsTypes } from './notifications.actions-types';
import { ErrorNotification } from './error-notification';
import { SuccessNotification } from './success-notification';
import { handleError } from '../../core/errors/handle-error';

@Service()
export class NotificationsEffects extends AbstractEffects {
    @Effect()
    protected onError: epic<
        ReturnType<typeof NotificationsActions['addError']>,
        PayloadAction<any, any>
    > = (action$) =>
        action$.pipe(
            ofType(notificationsActionsTypes.addError),
            map((action) => {
                const error = handleError(action.payload);
                const { ...message } = new ErrorNotification(error.reason);
                return NotificationsActions.addMessage(message);
            }),
        );

    @Effect()
    protected onSuccess: epic<
        ReturnType<typeof NotificationsActions['addSuccess']>,
        PayloadAction<any, any>
    > = (action$) =>
        action$.pipe(
            ofType(notificationsActionsTypes.addSuccess),
            map((action) => {
                const notification = new SuccessNotification(action.payload);
                return NotificationsActions.addMessage(notification);
            }),
        );

    @Effect()
    protected onMessage: epic<
        ReturnType<typeof NotificationsActions['addMessage']>,
        PayloadAction<any, any>
    > = (action$) =>
        action$.pipe(
            ofType(notificationsActionsTypes.addMessage),
            delay(5000),
            map((action) =>
                NotificationsActions.removeMessage(action.payload.id),
            ),
        );
}
