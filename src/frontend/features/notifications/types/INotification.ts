import { notificationsType } from './notificationsType';

export interface INotification {
    id: string;
    type: notificationsType;
    payload: string;
}
