import { createAction } from '@reduxjs/toolkit';
import { notificationsActionsTypes } from './notifications.actions-types';
import { INotification } from './types/INotification';
import { AbstractException } from '../../core/errors/exceptions/abstract.exception';
import { successNotifications } from '../notifications/enums/success-notifications';

export class NotificationsActions {
    public static addError = createAction<
        AbstractException,
        notificationsActionsTypes
    >(notificationsActionsTypes.addError);

    public static addSuccess = createAction<
        successNotifications,
        notificationsActionsTypes
    >(notificationsActionsTypes.addSuccess);

    public static addMessage = createAction<
        INotification,
        notificationsActionsTypes
    >(notificationsActionsTypes.addMessage);

    public static removeMessage = createAction<
        string,
        notificationsActionsTypes
    >(notificationsActionsTypes.removeMessage);

    public static clearMessages = createAction<void, notificationsActionsTypes>(
        notificationsActionsTypes.clearMessages,
    );
}
