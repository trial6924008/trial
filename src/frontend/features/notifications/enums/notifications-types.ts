export enum notificationsTypes {
    error = 'error',
    warning = 'warning',
    info = 'info',
    success = 'success',
}
