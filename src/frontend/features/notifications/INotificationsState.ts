import { INotification } from './types/INotification';

export interface INotificationsState {
    messages: INotification[];
}
