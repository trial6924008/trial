import { Notification } from './notification';
import { notificationsTypes } from './enums/notifications-types';
import { INotification } from './types/INotification';

export class ErrorNotification extends Notification implements INotification {
    constructor(payload: string) {
        super(notificationsTypes.error, payload);
    }
}
