import { Service, Inject } from 'typedi';
import { AbstractFeature } from '../../store/abstract.feature';
import { INotificationsState } from './INotificationsState';
import { NotificationsEffects } from './notifications.effects';
import { NotificationsReducer } from './notifications.reducer';

@Service()
export class NotificationsFeature extends AbstractFeature<INotificationsState> {
    constructor(
        @Inject() public override readonly effects: NotificationsEffects,
        @Inject() public override readonly reducer: NotificationsReducer,
    ) {
        super();
    }
}
