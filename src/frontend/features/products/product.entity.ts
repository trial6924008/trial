import { IsMongoId, IsString, MinLength, IsNumber } from 'class-validator';
import { IProduct } from '../../../shared/products/IProduct';

export class ProductEntity implements IProduct {
    @IsMongoId()
    id: string;

    @IsString()
    @MinLength(1)
    name: string;

    @IsNumber()
    price: number;
}
