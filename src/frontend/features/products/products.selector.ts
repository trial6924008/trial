import { createSelector } from '@reduxjs/toolkit';
import { selector } from '../../store/selector';
import { IRootState } from '../../store/IRootState';
import { IProductsState } from './IProductsState';

export const productsSelector: selector<IProductsState> = createSelector(
    (state: IRootState) => state,
    (state: IRootState) => state.products,
);
