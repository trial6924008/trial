import { ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { Service } from 'typedi';
import { AbstractReducer } from '../../store/abstract.reducer';
import { IProductsState } from './IProductsState';
import { ProductsActions } from './products.actions';

@Service()
export class ProductsReducer extends AbstractReducer<IProductsState> {
    protected override initialState = {
        loading: false,
        productsList: [],
    };

    protected override buildReducer(
        builder: ActionReducerMapBuilder<IProductsState>,
    ): void {
        builder
            .addCase(ProductsActions.setLoading, (state) => {
                state.loading = true;
            })
            .addCase(ProductsActions.resetLoading, (state) => {
                state.loading = false;
            })
            .addCase(ProductsActions.setProducts, (state, action) => {
                state.productsList = action.payload
                    .map(product => ({
                        ...product,
                        price: product.price / 100
                    }));
            })
            .addCase(ProductsActions.resetProducts, (state) => {
                state.productsList = [];
            });
    }
}
