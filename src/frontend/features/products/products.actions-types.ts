export enum productsActionsTypes {
    setLoading = '[products]_setLoading',
    resetLoading = '[products]_resetLoading',
    setProducts = '[products]_setProducts',
    resetProducts = '[products]_resetProducts',
    getProducts = '[products]_getProducts',
}
