import { IProduct } from '../../../shared/products/IProduct';

export interface IProductsState {
    loading: boolean;
    productsList: IProduct[];
}
