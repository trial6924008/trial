import { PayloadAction } from '@reduxjs/toolkit';
import { ofType } from 'redux-observable';
import type { Epic as epic } from 'redux-observable';
import { Service, Inject } from 'typedi';
import { of, defer } from 'rxjs';
import { map } from 'rxjs/operators';
import { Effect } from '../../store/effect.decorator';
import { AbstractEffects } from '../../store/abstract.effects';
import { IRootState } from '../../store/IRootState';
import { ProductsApi } from '../../services/api/products.api';
import { ProductsActions } from './products.actions';
import { productsActionsTypes } from './products.actions-types';
import { ProductEntity } from './product.entity';

@Service()
export class ProductsEffects extends AbstractEffects {
    constructor(
        @Inject() private readonly productsApi: ProductsApi,
    ) {
        super();
    }

    @Effect()
    protected onGetProducts: epic<
        ReturnType<typeof ProductsActions['getProducts']>,
        PayloadAction<any, productsActionsTypes>,
        IRootState
    > = (action$) =>
        action$.pipe(
            ofType(productsActionsTypes.getProducts),
            this.merge(
                of(ProductsActions.setLoading()),
                defer(() =>
                    this.productsApi.getAllProducts(),
                ).pipe(
                    this.validate(ProductEntity),
                    map((res) => ProductsActions.setProducts(res)),
                    this.catchError(),
                    this.afterAll(ProductsActions.resetLoading()),
                ),
            ),
        );
}
