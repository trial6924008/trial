import { createAction } from '@reduxjs/toolkit';
import { IProduct } from '../../../shared/products/IProduct';
import { productsActionsTypes } from './products.actions-types';

export class ProductsActions {
    public static setLoading = createAction<void, productsActionsTypes>(
        productsActionsTypes.setLoading,
    );

    public static resetLoading = createAction<void, productsActionsTypes>(
        productsActionsTypes.resetLoading,
    );

    public static setProducts = createAction<IProduct[], productsActionsTypes>(
        productsActionsTypes.setProducts,
    );

    public static resetProducts = createAction<void, productsActionsTypes>(
        productsActionsTypes.resetProducts,
    );

    public static getProducts = createAction<void, productsActionsTypes>(
        productsActionsTypes.getProducts,
    );
}
