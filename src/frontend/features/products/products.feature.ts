import { Service, Inject } from 'typedi';
import { AbstractFeature } from '../../store/abstract.feature';
import { IProductsState } from './IProductsState';
import { ProductsEffects } from './products.effects';
import { ProductsReducer } from './products.reducer';

@Service()
export class ProductsFeature extends AbstractFeature<IProductsState> {
    constructor(
        @Inject() public override readonly effects: ProductsEffects,
        @Inject() public override readonly reducer: ProductsReducer,
    ) {
        super();
    }
}
