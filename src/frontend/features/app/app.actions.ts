import { createAction } from '@reduxjs/toolkit';
import { appActionsTypes } from './app.actions-types';

export class AppActions {
    public static setLoading = createAction<void, appActionsTypes>(
        appActionsTypes.setLoading,
    );

    public static resetLoading = createAction<void, appActionsTypes>(
        appActionsTypes.resetLoading,
    );

    public static init = createAction<void, appActionsTypes>(
        appActionsTypes.init,
    );
}
