import { PayloadAction } from '@reduxjs/toolkit';
import { ofType } from 'redux-observable';
import type { Epic as epic } from 'redux-observable';
import { Service, Inject } from 'typedi';
import { of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { IRootState } from '../../store/IRootState';
import { Effect } from '../../store/effect.decorator';
import { AbstractEffects } from '../../store/abstract.effects';
import { AppActions } from './app.actions';
import { appActionsTypes } from './app.actions-types';
import { StandsActions } from '../stands/stands.actions';
import { standsActionsTypes } from '../stands/stands.actions-types';
import { productsActionsTypes } from '../products/products.actions-types';
import { ProductsActions } from '../products/products.actions';
import { TagsActions } from '../tags/tags.actions';

@Service()
export class AppEffects extends AbstractEffects {
    @Effect()
    protected onInit: epic<
        ReturnType<typeof AppActions['init']>,
        PayloadAction<any, any>,
        IRootState
    > = (action$) =>
        action$.pipe(
            ofType(appActionsTypes.init),
            mergeMap(() => {
                return of(
                    StandsActions.getStands(),
                    ProductsActions.getProducts(),
                    TagsActions.getTags(),
                );
            }),
        );

    @Effect()
    protected onStartLoading: epic<
        PayloadAction<void, appActionsTypes> |
        PayloadAction<void, productsActionsTypes> |
        PayloadAction<void, standsActionsTypes>,
        PayloadAction<void, appActionsTypes>
    > = (action$) =>
        action$.pipe(
            ofType<PayloadAction<any, any>, productsActionsTypes | standsActionsTypes>(productsActionsTypes.setLoading, standsActionsTypes.setLoading),
            map(() => AppActions.setLoading()),
        );

    @Effect()
    protected onStopLoading: epic<
        PayloadAction<void, appActionsTypes> |
        PayloadAction<void, productsActionsTypes> |
        PayloadAction<void, standsActionsTypes>,
        PayloadAction<void, appActionsTypes>
    > = (action$) =>
        action$.pipe(
            ofType<PayloadAction<any, any>, productsActionsTypes | standsActionsTypes>(productsActionsTypes.resetLoading, standsActionsTypes.resetLoading),
            map(() => AppActions.resetLoading()),
        );
}
