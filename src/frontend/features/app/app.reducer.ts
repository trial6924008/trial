import { ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { Service } from 'typedi';
import { AbstractReducer } from '../../store/abstract.reducer';
import { IAppState } from './IAppState';
import { AppActions } from './app.actions';

@Service()
export class AppReducer extends AbstractReducer<IAppState> {
    protected override initialState = {
        loading: false,
    };

    protected override buildReducer(
        builder: ActionReducerMapBuilder<IAppState>,
    ): void {
        builder
            .addCase(AppActions.setLoading, (state, action) => {
                state.loading = true;
            })
            .addCase(AppActions.resetLoading, (state, action) => {
                state.loading = false;
            });
    }
}
