export enum appActionsTypes {
    setLoading = 'setLoading',
    resetLoading = 'resetLoading',
    init = 'init',
}
