import { createSelector } from '@reduxjs/toolkit';
import { selector } from '../../store/selector';
import { IRootState } from '../../store/IRootState';
import { IAppState } from './IAppState';

export const appSelector: selector<IAppState> = createSelector(
    (state: IRootState) => state,
    (state: IRootState) => state.app,
);
