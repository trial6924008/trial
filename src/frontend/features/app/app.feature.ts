import { Service, Inject } from 'typedi';
import { AbstractFeature } from '../../store/abstract.feature';
import { AppEffects } from './app.effects';
import { AppReducer } from './app.reducer';
import { IAppState } from './IAppState';

@Service()
export class AppFeature extends AbstractFeature<IAppState> {
    constructor(
        @Inject() public override readonly effects: AppEffects,
        @Inject() public override readonly reducer: AppReducer,
    ) {
        super();
    }
}
