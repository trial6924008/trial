import { createAction } from '@reduxjs/toolkit';
import { ITag } from '../../../shared/tags/ITag';
import { tagsActionsTypes } from './tags.actions-types';

export class TagsActions {
    public static setLoading = createAction<void, tagsActionsTypes>(
        tagsActionsTypes.setLoading,
    );

    public static resetLoading = createAction<void, tagsActionsTypes>(
        tagsActionsTypes.resetLoading,
    );

    public static setTags = createAction<ITag[], tagsActionsTypes>(
        tagsActionsTypes.setTags,
    );

    public static resetTags = createAction<void, tagsActionsTypes>(
        tagsActionsTypes.resetTags,
    );

    public static getTags = createAction<void, tagsActionsTypes>(
        tagsActionsTypes.getTags,
    );
}
