import { Service, Inject } from 'typedi';
import { AbstractFeature } from '../../store/abstract.feature';
import { ITagsState } from './ITagsState';
import { TagsEffects } from './tags.effects';
import { TagsReducer } from './tags.reducer';

@Service()
export class TagsFeature extends AbstractFeature<ITagsState> {
    constructor(
        @Inject() public override readonly effects: TagsEffects,
        @Inject() public override readonly reducer: TagsReducer,
    ) {
        super();
    }
}
