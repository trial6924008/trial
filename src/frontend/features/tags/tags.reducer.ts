import { ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { Service } from 'typedi';
import { AbstractReducer } from '../../store/abstract.reducer';
import { ITagsState } from './ITagsState';
import { TagsActions } from './tags.actions';

@Service()
export class TagsReducer extends AbstractReducer<ITagsState> {
    protected override initialState = {
        loading: false,
        tagsList: [],
    };

    protected override buildReducer(
        builder: ActionReducerMapBuilder<ITagsState>,
    ): void {
        builder
            .addCase(TagsActions.setLoading, (state) => {
                state.loading = true;
            })
            .addCase(TagsActions.resetLoading, (state) => {
                state.loading = false;
            })
            .addCase(TagsActions.setTags, (state, action) => {
                state.tagsList = action.payload;
            })
            .addCase(TagsActions.resetTags, (state) => {
                state.tagsList = [];
            });
    }
}
