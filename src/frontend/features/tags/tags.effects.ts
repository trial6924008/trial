import { PayloadAction } from '@reduxjs/toolkit';
import { ofType } from 'redux-observable';
import type { Epic as epic } from 'redux-observable';
import { Service, Inject } from 'typedi';
import { of, defer } from 'rxjs';
import { map } from 'rxjs/operators';
import { Effect } from '../../store/effect.decorator';
import { AbstractEffects } from '../../store/abstract.effects';
import { IRootState } from '../../store/IRootState';
import { TagsApi } from '../../services/api/tags.api';
import { TagsActions } from './tags.actions';
import { tagsActionsTypes } from './tags.actions-types';
import { TagEntity } from './tags.entity';

@Service()
export class TagsEffects extends AbstractEffects {
    constructor(
        @Inject() private readonly tagsApi: TagsApi,
    ) {
        super();
    }

    @Effect()
    protected onGetStands: epic<
        ReturnType<typeof TagsActions['getTags']>,
        PayloadAction<any, tagsActionsTypes>,
        IRootState
    > = (action$) =>
        action$.pipe(
            ofType(tagsActionsTypes.getTags),
            this.merge(
                of(TagsActions.setLoading()),
                defer(() =>
                    this.tagsApi.getAllTags(),
                ).pipe(
                    this.validate(TagEntity),
                    map((res) => TagsActions.setTags(res)),
                    this.catchError(),
                    this.afterAll(TagsActions.resetLoading()),
                ),
            ),
        );
}
