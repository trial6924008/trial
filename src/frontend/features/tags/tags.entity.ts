import { IsMongoId, IsBoolean } from 'class-validator';
import { ITag } from '../../../shared/tags/ITag';

export class TagEntity implements ITag {
    @IsMongoId()
    id: string;

    @IsMongoId()
    productId: string;

    @IsBoolean()
    solded: boolean;

    @IsMongoId()
    standId: string;
}
