import { createSelector } from '@reduxjs/toolkit';
import { selector } from '../../store/selector';
import { IRootState } from '../../store/IRootState';
import { ITagsState } from './ITagsState';

export const tagsSelector: selector<ITagsState> = createSelector(
    (state: IRootState) => state,
    (state: IRootState) => state.tags,
);
