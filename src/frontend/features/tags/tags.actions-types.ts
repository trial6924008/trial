export enum tagsActionsTypes {
    setLoading = '[tags]_setLoading',
    resetLoading = '[tags]_resetLoading',
    setTags = '[tags]_setTags',
    resetTags = '[tags]_resetTags',
    getTags = '[tags]_getTags',
}
