import { ITag } from '../../../shared/tags/ITag';

export interface ITagsState {
    loading: boolean;
    tagsList: ITag[];
}
