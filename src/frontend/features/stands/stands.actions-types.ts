export enum standsActionsTypes {
    setLoading = '[stands]_setLoading',
    resetLoading = '[stands]_resetLoading',
    setStands = '[stands]_setStands',
    resetStands = '[stands]_resetStands',
    getStands = '[stands]_getStands',
}
