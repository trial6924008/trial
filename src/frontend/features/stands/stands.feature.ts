import { Service, Inject } from 'typedi';
import { AbstractFeature } from '../../store/abstract.feature';
import { IStandsState } from './IStandsState';
import { StandsEffects } from './stands.effects';
import { StandsReducer } from './stands.reducer';

@Service()
export class StandsFeature extends AbstractFeature<IStandsState> {
    constructor(
        @Inject() public override readonly effects: StandsEffects,
        @Inject() public override readonly reducer: StandsReducer,
    ) {
        super();
    }
}
