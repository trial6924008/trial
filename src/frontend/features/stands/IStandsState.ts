import { IStand } from '../../../shared/stands/IStand';

export interface IStandsState {
    loading: boolean;
    standsList: IStand[];
}
