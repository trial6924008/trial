import { ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { Service } from 'typedi';
import { AbstractReducer } from '../../store/abstract.reducer';
import { IStandsState } from './IStandsState';
import { StandsActions } from './stands.actions';

@Service()
export class StandsReducer extends AbstractReducer<IStandsState> {
    protected override initialState = {
        loading: false,
        standsList: [],
    };

    protected override buildReducer(
        builder: ActionReducerMapBuilder<IStandsState>,
    ): void {
        builder
            .addCase(StandsActions.setLoading, (state) => {
                state.loading = true;
            })
            .addCase(StandsActions.resetLoading, (state) => {
                state.loading = false;
            })
            .addCase(StandsActions.setStands, (state, action) => {
                state.standsList = action.payload;
            })
            .addCase(StandsActions.resetStands, (state) => {
                state.standsList = [];
            });
    }
}
