import { createSelector } from '@reduxjs/toolkit';
import { selector } from '../../store/selector';
import { IRootState } from '../../store/IRootState';
import { IStandsState } from './IStandsState';

export const standsSelector: selector<IStandsState> = createSelector(
    (state: IRootState) => state,
    (state: IRootState) => state.stands,
);
