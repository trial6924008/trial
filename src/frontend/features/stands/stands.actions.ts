import { createAction } from '@reduxjs/toolkit';
import { IStand } from '../../../shared/stands/IStand';
import { standsActionsTypes } from './stands.actions-types';

export class StandsActions {
    public static setLoading = createAction<void, standsActionsTypes>(
        standsActionsTypes.setLoading,
    );

    public static resetLoading = createAction<void, standsActionsTypes>(
        standsActionsTypes.resetLoading,
    );

    public static setStands = createAction<IStand[], standsActionsTypes>(
        standsActionsTypes.setStands,
    );

    public static resetStands = createAction<void, standsActionsTypes>(
        standsActionsTypes.resetStands,
    );

    public static getStands = createAction<void, standsActionsTypes>(
        standsActionsTypes.getStands,
    );
}
