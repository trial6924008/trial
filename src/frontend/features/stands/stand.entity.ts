import { IsMongoId, IsString, MinLength, IsBoolean, IsNumber } from 'class-validator';
import { IStand } from '../../../shared/stands/IStand';

export class StandEntity implements IStand {
    @IsMongoId()
    id: string;

    @IsString()
    address: string;

    @IsString()
    contacts: string;

    @IsString()
    @MinLength(1)
    name: string;

    @IsBoolean()
    online: boolean;

    @IsNumber()
    tagsCount: number;
}
