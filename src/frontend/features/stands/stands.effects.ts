import { PayloadAction } from '@reduxjs/toolkit';
import { ofType } from 'redux-observable';
import type { Epic as epic } from 'redux-observable';
import { Service, Inject } from 'typedi';
import { of, defer } from 'rxjs';
import { map } from 'rxjs/operators';
import { Effect } from '../../store/effect.decorator';
import { AbstractEffects } from '../../store/abstract.effects';
import { IRootState } from '../../store/IRootState';
import { StandsApi } from '../../services/api/stands.api';
import { StandsActions } from './stands.actions';
import { standsActionsTypes } from './stands.actions-types';
import { StandEntity } from './stand.entity';

@Service()
export class StandsEffects extends AbstractEffects {
    constructor(
        @Inject() private readonly standsApi: StandsApi,
    ) {
        super();
    }

    @Effect()
    protected onGetStands: epic<
        ReturnType<typeof StandsActions['getStands']>,
        PayloadAction<any, standsActionsTypes>,
        IRootState
    > = (action$) =>
        action$.pipe(
            ofType(standsActionsTypes.getStands),
            this.merge(
                of(StandsActions.setLoading()),
                defer(() =>
                    this.standsApi.getAllStands(),
                ).pipe(
                    this.validate(StandEntity),
                    map((res) => StandsActions.setStands(res)),
                    this.catchError(),
                    this.afterAll(StandsActions.resetLoading()),
                ),
            ),
        );
}
