import React from 'react';
import Box from '@mui/material/Box';
import { useCursor } from '../../app/hooks/useCursor';
import { NotificationsBarWidget } from '../../widgets/notifications-bar/notifications-bar.widget';
import { ILayout } from '../ILayout';
import styles from './app.module.scss';

const Layout: React.FC<ILayout> = ({ children }: ILayout) => {
    const cursor = useCursor();

    return (
        <Box className={styles['app']} sx={{ cursor }}>
            <NotificationsBarWidget />
            {children}
        </Box>
    );
};

export const AppLayout = React.memo(Layout);
