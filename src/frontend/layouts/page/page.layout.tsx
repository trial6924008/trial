import React from 'react';
import Box from '@mui/material/Box';
import { ILayout } from '../ILayout';
import styles from './page.module.scss';

const Layout: React.FC<ILayout> = ({ children }: ILayout) => {
    return (
        <Box className={styles['page']}>
            {children}
        </Box>
    );
};

export const PageLayout = React.memo(Layout);
