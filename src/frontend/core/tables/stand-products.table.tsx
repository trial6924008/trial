import { IProduct } from '../../../shared/products/IProduct';
import { AbstractTable } from './abstract.table';
import { IColumn } from './types/IColumn';
import { IRow } from './types/IRow';

interface IProductRow extends Pick<IProduct, 'name' | 'price'> {
    number: number;
}

interface IStandsList {
    columns: IColumn[];
    productsList: IProductRow[];
}

export class StandProductsTable extends AbstractTable {
    private readonly productsList: IProductRow[];

    constructor({ columns, productsList }: IStandsList) {
        super(columns);
        this.productsList = productsList;
        this.setRows();
    }

    private rowPipe(item: IProductRow): IRow {
        const keys = Object.keys(item) as Array<keyof typeof item>;
        return keys
        .filter((key) => this.columnsKeys.includes(key as keyof IColumn))
        .reduce(
            (acc, key) => {
                const _val = item[key];
                let val: string;
                switch (typeof _val) {
                    case 'number':
                        val = String(_val);
                        break;
                    default:
                        val = _val;
                        break;
                }
                return {
                    ...acc,
                    [key]: val,
                }
            },
            {},
        );
    }

    protected override checkValues() {
        if (!this.columns || !this.productsList)
            throw 'Products table error';
    }

    protected override getRows(): IRow[] {
        return this.productsList.map((item) => this.rowPipe(item));
    }
}
