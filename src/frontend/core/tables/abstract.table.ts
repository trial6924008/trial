import { IColumn } from './types/IColumn';
import { IRow } from './types/IRow';

export abstract class AbstractTable {
    protected readonly columns: IColumn[];
    protected readonly columnsKeys: (keyof IColumn)[]
    public rows: IRow[];

    constructor(columns: IColumn[]) {
        this.columns = columns;
        this.columnsKeys = this.columns.map(({ field }) => field as keyof IColumn);
    }

    protected abstract checkValues(): void;

    protected abstract getRows(): IRow[];

    protected setRows() {
        this.checkValues();
        this.rows = this.getRows();
    }
}
