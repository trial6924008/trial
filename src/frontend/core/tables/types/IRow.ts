import React from 'react';

export interface IRow {
    [key: string]: string | React.ReactElement;
}
