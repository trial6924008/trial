export interface IColumn {
    field: string;
    headerName: string;
}
