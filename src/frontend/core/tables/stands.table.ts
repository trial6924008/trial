import { IStand } from '../../../shared/stands/IStand';
import { AbstractTable } from './abstract.table';
import { IColumn } from './types/IColumn';
import { IRow } from './types/IRow';

interface IStandsList {
    columns: IColumn[];
    standsList: IStand[];
}

export class StandsTable extends AbstractTable {
    private readonly standsList: IStand[];

    constructor({ columns, standsList }: IStandsList) {
        super(columns);
        this.standsList = standsList;
        this.setRows();
    }

    private rowPipe(item: IStand): IRow {
        const keys = Object.keys(item) as Array<keyof typeof item>;
        return keys
        .filter((key) => this.columnsKeys.includes(key as keyof IColumn))
        .reduce(
            (acc, key) => {
                const _val = item[key];
                let val: string;
                switch (typeof _val) {
                    case 'boolean':
                        if (key === 'online') {
                            val = _val ? 'Online' : 'Offline';
                        } else {
                            val = _val ? 'Yes' : 'No';
                        }
                        break;
                    case 'number':
                        val = String(_val);
                        break;
                    default:
                        val = _val;
                        break;
                }
                return {
                    ...acc,
                    [key]: val,
                }
            },
            {},
        );
    }

    protected override checkValues() {
        if (!this.columns || !this.standsList)
            throw 'Stands table error';
    }

    protected override getRows(): IRow[] {
        return this.standsList.map((item) => this.rowPipe(item));
    }
}
