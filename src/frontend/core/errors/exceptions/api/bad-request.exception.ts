import { AbstractException } from '../abstract.exception';
import { apiExceptions } from './api.exceptions';

export class BadRequestException extends AbstractException {
    public readonly reason: string = apiExceptions.badRequest;
}
