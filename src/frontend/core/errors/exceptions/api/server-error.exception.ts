import { AbstractException } from '../abstract.exception';
import { apiExceptions } from './api.exceptions';

export class ServerErrorException extends AbstractException {
    public readonly reason: string = apiExceptions.serverError;
}
