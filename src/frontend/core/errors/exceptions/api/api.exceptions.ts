export enum apiExceptions {
    badRequest = 'Bad request',
    notFound = 'Not found',
    conflict = 'Conflict',
    serverError = 'Server error',
}
