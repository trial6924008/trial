import { AbstractException } from '../abstract.exception';
import { apiExceptions } from './api.exceptions';

export class ConflictException extends AbstractException {
    public readonly reason: string = apiExceptions.conflict;
}
