import { AbstractException } from '../abstract.exception';
import { apiExceptions } from './api.exceptions';

export class NotFoundException extends AbstractException {
    public readonly reason: string = apiExceptions.notFound;
}
