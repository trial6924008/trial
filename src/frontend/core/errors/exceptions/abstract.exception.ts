export abstract class AbstractException {
    public abstract readonly reason: string;
}
