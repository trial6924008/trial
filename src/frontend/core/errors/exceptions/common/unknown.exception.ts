import { AbstractException } from '../abstract.exception';
import { commonExceptions } from './common.exceptions';

export class UnknownException extends AbstractException {
    public readonly reason: string = commonExceptions.unknownError;
}
