import { BadRequestException } from './exceptions/api/bad-request.exception';
import { ConflictException } from './exceptions/api/conflict.exception';
import { NotFoundException } from './exceptions/api/not-found.exception';
import { UnknownException } from './exceptions/common/unknown.exception';

export function handleError(e: unknown) {
    const isCustomErr =
        e instanceof BadRequestException ||
        e instanceof ConflictException ||
        e instanceof NotFoundException ||
        e instanceof UnknownException
    if (isCustomErr) {
        return e;
    }
    return new UnknownException();
}
