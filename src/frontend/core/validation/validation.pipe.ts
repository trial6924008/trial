import { validate } from 'class-validator';
import { ClassConstructor, plainToInstance } from 'class-transformer';
import { Service } from 'typedi';

@Service()
export class ValidationPipe {
    private toValidate(metatype: Function): boolean {
        const types: Function[] = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }

    public async validate<T extends object = object>(
        value: T,
        metatype: ClassConstructor<any>,
    ): Promise<T> {
        if (!value || typeof value !== 'object') {
            throw new Error('not object');
        }
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }
        const object = plainToInstance(metatype, value);
        const errors = await validate(object);
        if (errors.length > 0) {
            const reason = errors
                .map(({ property }) => property)
                .filter((property, i, arr) => arr.indexOf(property) === i)
                .map((property) => `${property} field is not valid`);
            throw new Error(...reason);
        }
        return value as T;
    }
}
