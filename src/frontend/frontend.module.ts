import { Module } from '@nestjs/common';
import { RenderModule } from 'nest-next';
import Next from 'next';
import { resolve } from 'path';

@Module({
    imports: [
        RenderModule.forRootAsync(
            Next({
                dir: resolve(__dirname, '../..'),
            }),
            { viewsDir: resolve(__dirname, '../pages'), passthrough404: true }
        ),
    ],
    controllers: []
})
export class FrontendModule {}
