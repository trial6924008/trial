import { Service, Inject } from 'typedi';
import { HttpProvider } from '../../providers/http/http.provider';
import { apiMethods } from '../../providers/http/api-methods';
import { apiRoutes } from '../../../shared/common/api-routes'
import { ICreateProduct } from '../../../shared/products/ICreateProduct'
import { IProduct } from '../../../shared/products/IProduct';
import { IGetProduct } from '../../../shared/products/IGetProduct';
import { IEditProduct } from '../../../shared/products/IEditProduct';
import { IDeleteProduct } from '../../../shared/products/IDeleteProduct';

@Service()
export class ProductsApi {
    constructor(
        @Inject() private readonly http: HttpProvider
    ) {}

    public async createProduct(dto: ICreateProduct): Promise<IProduct> {
        return await this.http.request({
            pathname: `${apiRoutes.products}`,
            method: apiMethods.post,
            body: dto
        });
    }

    public async getProduct(dto: IGetProduct): Promise<IProduct> {
        return await this.http.request({
            pathname: `${apiRoutes.products}/${dto.id}`,
            method: apiMethods.get,
        });
    }

    public async getAllProducts(): Promise<IProduct[]> {
        return await this.http.request({
            pathname: `${apiRoutes.products}`,
            method: apiMethods.get,
        });
    }

    public async editProduct(dto: IEditProduct): Promise<IProduct> {
        const { id } = dto;
        const _dto = dto as Partial<IEditProduct>;
        delete _dto.id;
        const body = _dto as Omit<IEditProduct, 'id'>;
        return await this.http.request({
            pathname: `${apiRoutes.products}/${dto.id}`,
            method: apiMethods.put,
            body
        });
    }

    public async deleteProduct(dto: IDeleteProduct): Promise<void> {
        return await this.http.request({
            pathname: `${apiRoutes.products}/${dto.id}`,
            method: apiMethods.delete,
        });
    }
}
