import { Service, Inject } from 'typedi';
import { HttpProvider } from '../../providers/http/http.provider';
import { apiMethods } from '../../providers/http/api-methods';
import { apiRoutes } from '../../../shared/common/api-routes'
import { ITag } from '../../../shared/tags/ITag';
import { ICreateTag } from '../../../shared/tags/ICreateTag';
import { IGetTag } from '../../../shared/tags/IGetTag';
import { IEditTag } from '../../../shared/tags/IEditTag';
import { IDeleteTag } from '../../../shared/tags/IDeleteTag';

@Service()
export class TagsApi {
    constructor(
        @Inject() private readonly http: HttpProvider
    ) {}

    public async createTag(dto: ICreateTag): Promise<ITag> {
        return await this.http.request({
            pathname: `${apiRoutes.tags}`,
            method: apiMethods.post,
            body: dto
        });
    }

    public async getTag(dto: IGetTag): Promise<ITag> {
        return await this.http.request({
            pathname: `${apiRoutes.tags}/${dto.id}`,
            method: apiMethods.get,
        });
    }

    public async getAllTags(): Promise<ITag[]> {
        return await this.http.request({
            pathname: `${apiRoutes.tags}`,
            method: apiMethods.get,
        });
    }

    public async editTag(dto: IEditTag): Promise<ITag> {
        const { id } = dto;
        const _dto = dto as Partial<IEditTag>;
        delete _dto.id;
        const body = _dto as Omit<IEditTag, 'id'>;
        return await this.http.request({
            pathname: `${apiRoutes.tags}/${dto.id}`,
            method: apiMethods.put,
            body
        });
    }

    public async deleteTag(dto: IDeleteTag): Promise<void> {
        return await this.http.request({
            pathname: `${apiRoutes.tags}/${dto.id}`,
            method: apiMethods.delete,
        });
    }
}
