import { Service, Inject } from 'typedi';
import { HttpProvider } from '../../providers/http/http.provider';
import { apiMethods } from '../../providers/http/api-methods';
import { apiRoutes } from '../../../shared/common/api-routes'
import { IStand } from '../../../shared/stands/IStand';
import { ICreateStand } from '../../../shared/stands/ICreateStand';
import { IGetStand } from '../../../shared/stands/IGetStand';
import { IEditStand } from '../../../shared/stands/IEditStand';
import { IDeleteStand } from '../../../shared/stands/IDeleteStand';

@Service()
export class StandsApi {
    constructor(
        @Inject() private readonly http: HttpProvider
    ) {}

    public async createStand(dto: ICreateStand): Promise<IStand> {
        return await this.http.request({
            pathname: `${apiRoutes.stands}`,
            method: apiMethods.post,
            body: dto
        });
    }

    public async getStand(dto: IGetStand): Promise<IStand> {
        return await this.http.request({
            pathname: `${apiRoutes.stands}/${dto.id}`,
            method: apiMethods.get,
        });
    }

    public async getAllStands(): Promise<IStand[]> {
        return await this.http.request({
            pathname: `${apiRoutes.stands}`,
            method: apiMethods.get,
        });
    }

    public async editStand(dto: IEditStand): Promise<IStand> {
        const { id } = dto;
        const _dto = dto as Partial<IEditStand>;
        delete _dto.id;
        const body = _dto as Omit<IEditStand, 'id'>;
        return await this.http.request({
            pathname: `${apiRoutes.stands}/${dto.id}`,
            method: apiMethods.put,
            body
        });
    }

    public async deleteStand(dto: IDeleteStand): Promise<void> {
        return await this.http.request({
            pathname: `${apiRoutes.stands}/${dto.id}`,
            method: apiMethods.delete,
        });
    }
}
