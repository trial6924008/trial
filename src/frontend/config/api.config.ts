import { Service } from 'typedi';

interface IApiConfig {
    host: string;
    port: number;
    encryption: boolean;
}

@Service()
export class ApiConfig implements IApiConfig {
    public readonly host: string;
    public readonly port: number;
    public readonly encryption: boolean;

    constructor() {
        const values = this.getValues();
        this.host = values.host;
        this.port = values.port;
        this.encryption = values.encryption;
    }

    private getValues(): IApiConfig {
        if (!process.env.NEXT_PUBLIC_API_HOST) throw `empty host`;
        if (!process.env.NEXT_PUBLIC_API_PORT) throw `empty port`;
        if (!process.env.NEXT_PUBLIC_API_ENCRYPTION) throw `empty encryption`;
        const host = process.env.NEXT_PUBLIC_API_HOST!;
        const port = parseInt(process.env.NEXT_PUBLIC_API_PORT!, 10);
        const encryption = process.env.NEXT_PUBLIC_API_ENCRYPTION === 'true';
        return {
            host,
            port,
            encryption,
        };
    }
}
