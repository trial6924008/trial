import { PayloadAction } from '@reduxjs/toolkit';
import { Epic } from 'redux-observable';
import { ClassConstructor } from 'class-transformer';
import { from, Observable, of, merge, OperatorFunction } from 'rxjs';
import { catchError, mergeMap, connect, map, filter } from 'rxjs/operators';
import { Inject } from 'typedi';
import { ValidationPipe } from '../core/validation/validation.pipe';
import { handleError } from '../core/errors/handle-error';
import { AbstractException } from '../core/errors/exceptions/abstract.exception';
import { notificationsActionsTypes } from '../features/notifications/notifications.actions-types';
import { NotificationsActions } from '../features/notifications/notifications.actions';

type validationPipe<T extends object> = (res: T) => Observable<T>;

export abstract class AbstractEffects {
    @Inject()
    protected readonly validationPipe!: ValidationPipe;

    protected merge(...actions: Observable<PayloadAction<any, any>>[]) {
        return mergeMap(() => merge(...actions));
    }

    protected emptyAction(): PayloadAction<null, string> {
        return { type: '', payload: null };
    }

    protected validate$<T extends object>(
        metatype: ClassConstructor<any>,
    ): validationPipe<T> {
        return (res: T) => from(this.validationPipe.validate(res, metatype));
    }

    protected validate<T extends object>(metatype: ClassConstructor<any>) {
        return mergeMap<T, Observable<T>>(this.validate$(metatype));
    }

    protected afterAll(
        action: PayloadAction<any, any>,
    ): OperatorFunction<PayloadAction<any, any>, PayloadAction<any, any>> {
        return connect((res$) =>
            merge(
                res$.pipe(filter((res) => !!res)),
                res$.pipe(map(() => action)),
            ),
        );
    }

    protected catchError(): OperatorFunction<
        any,
        PayloadAction<AbstractException, notificationsActionsTypes>
    > {
        return catchError<
            any,
            Observable<
                PayloadAction<AbstractException, notificationsActionsTypes>
            >
        >((err) => {
            const error = handleError(err);
            return of(NotificationsActions.addError(error));
        });
    }

    public getEpics(): Epic[] {
        let keys: (keyof AbstractEffects)[] = [];
        const list = Reflect.getMetadata('effects', this);
        if (Array.isArray(list)) {
            keys = [...list];
        }
        const result = keys.map(
            (key) => this[key as keyof AbstractEffects] as any,
        );
        return result as Epic[];
    }
}
