import { IRootState } from './IRootState';

export type selector<T> = (state: IRootState) => T;
