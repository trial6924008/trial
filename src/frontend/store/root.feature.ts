import { combineReducers, PayloadAction, ReducersMapObject } from '@reduxjs/toolkit';
import {
    Epic,
    EpicMiddleware,
    createEpicMiddleware,
    combineEpics,
} from 'redux-observable';
import { Service, Inject } from 'typedi';
import { AbstractEffects } from './abstract.effects';
import { IRootState } from './IRootState';
import { AppFeature } from '../features/app/app.feature';
import { NotificationsFeature } from '../features/notifications/notifications.feature';
import { ProductsFeature } from '../features/products/products.feature';
import { StandsFeature } from '../features/stands/stands.feature';
import { TagsFeature } from '../features/tags/tags.feature';

@Service()
export class RootFeature {
    private readonly reducers: ReducersMapObject<IRootState>;
    private readonly effects: AbstractEffects[];
    public readonly middleware: EpicMiddleware<PayloadAction, PayloadAction, IRootState>;

    constructor(
        @Inject() private readonly appFeature: AppFeature,
        @Inject()
        private readonly notificationsFeatureature: NotificationsFeature,
        @Inject() private readonly productsFeature: ProductsFeature,
        @Inject() private readonly standsFeature: StandsFeature,
        @Inject() private readonly tagsFeature: TagsFeature,
    ) {
        const featuresMap = {
            app: this.appFeature,
            notifications: this.notificationsFeatureature,
            products: this.productsFeature,
            stands: this.standsFeature,
            tags: this.tagsFeature,
        };

        const features = Object.values(featuresMap);

        this.reducers = {
            app: this.appFeature.reducer.createReducer(),
            notifications:
                this.notificationsFeatureature.reducer.createReducer(),
            products: this.productsFeature.reducer.createReducer(),
            stands: this.standsFeature.reducer.createReducer(),
            tags: this.tagsFeature.reducer.createReducer(),
        };

        this.effects = features
            .filter(({effects}) => !!effects)
            .map(({effects}) => effects!);

        this.middleware = createEpicMiddleware();
    }

    public createReducer() {
        return combineReducers(this.reducers);
    }

    private getEpics(): Epic[] {
        return this.effects.reduce(
            (acc: Epic[], val) => [...acc, ...val.getEpics()],
            [],
        );
    }

    public start() {
        const epics = this.getEpics();
        const epic = combineEpics(...epics);
        this.middleware.run(epic);
    }
}
