import { IAppState } from '../features/app/IAppState';
import { INotificationsState } from '../features/notifications/INotificationsState';
import { IProductsState } from '../features/products/IProductsState';
import { IStandsState } from '../features/stands/IStandsState';
import { ITagsState } from '../features/tags/ITagsState';

export interface IRootState {
    app: IAppState;
    notifications: INotificationsState;
    products: IProductsState;
    stands: IStandsState;
    tags: ITagsState;
}
