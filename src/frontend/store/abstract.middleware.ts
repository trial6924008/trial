import type { Middleware } from '@reduxjs/toolkit';

export abstract class AbstractMiddleware {
    public abstract readonly middleware: Middleware;

    public start() {}
}
