export function Effect() {
    return function (target: any, propertyKey: string) {
        let result: string[] = [];
        const list = Reflect.getMetadata('effects', target);
        if (Array.isArray(list)) {
            result = [...list];
        }
        Reflect.defineMetadata('effects', [...result, propertyKey], target);
    };
}
