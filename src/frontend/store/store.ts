import {
    configureStore,
    Reducer,
    EnhancedStore,
} from '@reduxjs/toolkit';
import { CurriedGetDefaultMiddleware } from '@reduxjs/toolkit/dist/getDefaultMiddleware';
import { Service, Inject } from 'typedi';
import { IRootState } from './IRootState';
import { AppActions } from '../features/app/app.actions';
import { RootFeature } from './root.feature';
import { AbstractMiddleware } from './abstract.middleware';

@Service()
export class Store {
    public readonly value: EnhancedStore<IRootState>;

    constructor(@Inject() private readonly rootFeature: RootFeature) {
        const reducer: Reducer<IRootState> = this.rootFeature.createReducer();
        const middlewareList: AbstractMiddleware[] = [this.rootFeature];
        const middleware = (
            getDefaultMiddleware: CurriedGetDefaultMiddleware,
        ) => {
            const list = middlewareList.map(({ middleware }) => middleware);
            return getDefaultMiddleware({ serializableCheck: false }).concat(
                list,
            );
        };
        this.value = configureStore({
            reducer,
            middleware,
        });
        middlewareList.forEach((mw) => mw.start());
        this.value.dispatch(AppActions.init());
    }
}
