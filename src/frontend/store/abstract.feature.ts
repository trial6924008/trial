import { AbstractEffects } from './abstract.effects';
import { AbstractReducer } from './abstract.reducer';

export abstract class AbstractFeature<S extends object> {
    public readonly effects?: AbstractEffects;
    public readonly reducer?: AbstractReducer<S>;
}
