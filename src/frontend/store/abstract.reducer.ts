import { createReducer, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { ReducerWithInitialState } from '@reduxjs/toolkit/dist/createReducer';

export abstract class AbstractReducer<S extends object> {
    protected abstract buildReducer(builder: ActionReducerMapBuilder<S>): void;
    protected abstract initialState: S;

    protected getState(): S {
        return this.initialState;
    }

    public reducer: ReducerWithInitialState<S>;

    public createReducer() {
        this.reducer = createReducer(
            this.getState(),
            this.buildReducer.bind(this),
        );
        return this.reducer;
    }
}
