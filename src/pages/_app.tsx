import '../frontend/styles/global.scss';
import 'reflect-metadata';
import type { AppProps } from 'next/app';
import { Container } from 'typedi';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@mui/material/styles';
import { SnackbarProvider } from 'notistack';
import { MainTheme } from '../frontend/app/themes/main.theme';
import { Store } from '../frontend/store/store';
import { AppLayout } from '../frontend/layouts/app/app.layout';

export default function App({ Component, pageProps }: AppProps) {
    return (
        <Provider store={Container.get(Store).value}>
            <ThemeProvider theme={Container.get(MainTheme).theme}>
                <SnackbarProvider>
                    <AppLayout>
                        <Component {...pageProps} />
                    </AppLayout>
                </SnackbarProvider>
            </ThemeProvider>
        </Provider>
    );
}
