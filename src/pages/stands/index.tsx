import React from 'react';
import Head from 'next/head';
import { StandsListWidget } from '../../frontend/widgets/stands-list/stands-list.widget';
import { PageLayout } from '../../frontend/layouts/page/page.layout';

const Page: React.FC = () => {
    return (
        <>
            <Head>
                <title>Stands</title>
            </Head>
            <PageLayout>
                <StandsListWidget />
            </PageLayout>
        </>
    );
}

const StandsPage = React.memo(Page);

export default StandsPage;
