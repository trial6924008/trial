import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { StandProductsListWidget } from '../../frontend/widgets/stand-products-list/stand-products-list.widget';
import { SoldProductsListWidget } from '../../frontend/widgets/sold-products-list/sold-products-list.widget';
import { PageLayout } from '../../frontend/layouts/page/page.layout';
import { StandInfoWidget } from '../../frontend/widgets/stand-info/stand-info.widget';
import { standsSelector } from '../../frontend/features/stands/stands.selector';
import { BackMenuWidget } from '../../frontend/widgets/back-menu/back-menu.widget';

const Page: React.FC = () => {
    const router = useRouter();
    const { standId } = router.query;
    const { standsList } = useSelector(standsSelector);

    const standName = useMemo(
        () => standsList
            .filter(({ id }) => id === standId)
            .map(({ name }) => name)
            .shift() ?? '', 
        [standsList, standId]
    );

    return (
        <>
            <Head>
                <title>{`Stand${standName ? ` – ${standName}`: ''}`}</title>
            </Head>
            <PageLayout>
                <BackMenuWidget />
                <StandInfoWidget />
                <StandProductsListWidget />
                <SoldProductsListWidget />
            </PageLayout>
        </>
    );
}

const StandPage = React.memo(Page);

export default StandPage;
