import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsString, MinLength, IsNumber } from 'class-validator';
import { IProduct } from '../../shared/products/IProduct';

export class ProductEntity implements IProduct {
    @IsMongoId()
    @ApiProperty({ description: 'ID' })
    id: string;

    @IsString()
    @MinLength(1)
    @ApiProperty({ description: 'Name of product' })
    name: string;

    @IsNumber()
    @ApiProperty({ description: 'Price of product in cents' })
    price: number;
}
