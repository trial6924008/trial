import { Inject, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Controller } from '../../core/rest/decorators/controller.decorator';
import { apiRoutes } from '../../shared/common/api-routes';
import { CreateProductDto } from '../dto/create-product.dto';
import { DeleteProductDto } from '../dto/delete-product.dto';
import { GetProductDto } from '../dto/get-product.dto';
import { UpdateProductDto } from '../dto/update-product.dto';
import { ProductEntity } from '../entities/product.entity';
import { ProductsService } from '../services/products.service';

@Controller(apiRoutes.products)
export class ProductsController {
    constructor(
        @Inject(ProductsService) private readonly service: ProductsService,
    ) {}

    @Post()
    @ApiOperation({ summary: 'create product' })
    public async createProduct(
        @Body() dto: CreateProductDto,
    ): Promise<ProductEntity> {
        return await this.service.createProduct(dto);
    }

    @Get(':id')
    @ApiOperation({ summary: 'get product by ID' })
    public async getProduct(
        @Param() dto: GetProductDto,
    ): Promise<ProductEntity> {
        return await this.service.getProduct(dto);
    }

    @Get()
    @ApiOperation({ summary: 'get all products' })
    public async getAllProducts(): Promise<ProductEntity[]> {
        return await this.service.getAllProducts();
    }

    @Put(':id')
    @ApiOperation({ summary: 'edit product' })
    public async editProduct(
        @Param() dto: GetProductDto,
        @Body() update: UpdateProductDto,
    ): Promise<ProductEntity> {
        return await this.service.editProduct({ ...dto, ...update });
    }

    @Delete(':id')
    @ApiOperation({ summary: 'delete product' })
    public async deleteProduct(@Param() dto: DeleteProductDto): Promise<void> {
        return await this.service.deleteProduct(dto);
    }
}
