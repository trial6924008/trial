import { Injectable, Inject } from '@nestjs/common';
import { handleError } from '../../core/errors/handle-error';
import { IProductsService } from '../../shared/products/IProductsService';
import { CreateProductDto } from '../dto/create-product.dto';
import { DeleteProductDto } from '../dto/delete-product.dto';
import { EditProductDto } from '../dto/edit-product.dto';
import { GetProductDto } from '../dto/get-product.dto';
import { ProductEntity } from '../entities/product.entity';
import { ProductsRepository } from '../repositories/products.repository';

@Injectable()
export class ProductsService implements IProductsService {
    constructor(
        @Inject(ProductsRepository)
        private readonly repository: ProductsRepository,
    ) {}

    public async createProduct(dto: CreateProductDto): Promise<ProductEntity> {
        try {
            return await this.repository.create(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async getProduct(dto: GetProductDto): Promise<ProductEntity> {
        try {
            return await this.repository.readOne(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async getAllProducts(): Promise<ProductEntity[]> {
        try {
            return await this.repository.readAll();
        } catch (e) {
            throw handleError(e);
        }
    }

    public async editProduct(dto: EditProductDto): Promise<ProductEntity> {
        try {
            return await this.repository.update(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async deleteProduct(dto: DeleteProductDto): Promise<void> {
        try {
            return await this.repository.delete(dto);
        } catch (e) {
            throw handleError(e);
        }
    }
}
