import { PickType } from '@nestjs/swagger';
import { ICreateProduct } from '../../shared/products/ICreateProduct';
import { ProductEntity } from '../entities/product.entity';

export class CreateProductDto
    extends PickType(ProductEntity, ['name', 'price'] as const)
    implements ICreateProduct {}
