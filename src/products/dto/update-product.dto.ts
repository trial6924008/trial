import { OmitType, PartialType } from '@nestjs/swagger';
import { IEditProduct } from '../../shared/products/IEditProduct';
import { ProductEntity } from '../entities/product.entity';

export class UpdateProductDto
    extends PartialType(OmitType(ProductEntity, ['id'] as const))
    implements Omit<IEditProduct, 'id'> {}
