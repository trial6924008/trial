import { IntersectionType } from '@nestjs/swagger';
import { IEditProduct } from '../../shared/products/IEditProduct';
import { GetProductDto } from './get-product.dto';
import { UpdateProductDto } from './update-product.dto';

export class EditProductDto
    extends IntersectionType(GetProductDto, UpdateProductDto)
    implements IEditProduct {}
