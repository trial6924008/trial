import { IDeleteProduct } from '../../shared/products/IDeleteProduct';
import { GetProductDto } from './get-product.dto';

export class DeleteProductDto extends GetProductDto implements IDeleteProduct {}
