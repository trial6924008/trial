import { PickType } from '@nestjs/swagger';
import { IGetProduct } from '../../shared/products/IGetProduct';
import { ProductEntity } from '../entities/product.entity';

export class GetProductDto
    extends PickType(ProductEntity, ['id'] as const)
    implements IGetProduct {}
