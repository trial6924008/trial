import {
    Injectable,
    Inject,
    ConflictException,
    NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { CreateProductDto } from '../dto/create-product.dto';
import { DeleteProductDto } from '../dto/delete-product.dto';
import { EditProductDto } from '../dto/edit-product.dto';
import { GetProductDto } from '../dto/get-product.dto';
import { ProductEntity } from '../entities/product.entity';

@Injectable()
export class ProductsRepository {
    constructor(
        @Inject(PrismaService) private readonly prisma: PrismaService,
    ) {}

    private async checkIdExist(id: string) {
        const result = await this.prisma.product.findUnique({
            where: {
                id,
            },
            select: {
                id: true,
            },
        });
        return !!result;
    }

    private async checkNameExist(name: string) {
        const result = await this.prisma.product.findUnique({
            where: {
                name,
            },
            select: {
                id: true,
            },
        });
        return !!result;
    }

    public async readOne(dto: GetProductDto): Promise<ProductEntity> {
        const result = await this.prisma.product.findUnique({
            where: dto,
        });
        if (!result) {
            throw new NotFoundException();
        }
        return result;
    }

    public async readAll(): Promise<ProductEntity[]> {
        return await this.prisma.product.findMany({});
    }

    public async create(dto: CreateProductDto): Promise<ProductEntity> {
        const isExist = await this.checkNameExist(dto.name);
        if (isExist) {
            throw new ConflictException();
        }
        return await this.prisma.product.create({
            data: dto,
        });
    }

    public async update(dto: EditProductDto): Promise<ProductEntity> {
        const isIdExist = await this.checkIdExist(dto.id);
        if (!isIdExist) {
            throw new NotFoundException();
        }
        if (dto.name) {
            const isNameExist = await this.checkNameExist(dto.name);
            if (isNameExist) {
                throw new ConflictException();
            }
        }
        const _dto = { ...dto } as Partial<EditProductDto>;
        delete _dto.id;
        const update = _dto as Omit<EditProductDto, 'id'>;
        return await this.prisma.product.update({
            where: { id: dto.id },
            data: update,
        });
    }

    public async delete(dto: DeleteProductDto): Promise<void> {
        const isExist = await this.checkIdExist(dto.id);
        if (!isExist) {
            throw new NotFoundException();
        }
        await this.prisma.product.delete({
            where: dto,
        });
    }
}
