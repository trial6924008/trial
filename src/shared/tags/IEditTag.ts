import { IGetTag } from './IGetTag';
import { ITag } from './ITag';

export interface IEditTag extends IGetTag, Partial<Pick<ITag, 'solded'>> {}
