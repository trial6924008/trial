import { ICreateTag } from './ICreateTag';
import { IDeleteTag } from './IDeleteTag';
import { IEditTag } from './IEditTag';
import { IGetTag } from './IGetTag';
import { ITag } from './ITag';

export interface ITagsService {
    createTag(dto: ICreateTag): Promise<ITag>;
    getTag(dto: IGetTag): Promise<ITag>;
    getAllTags(): Promise<ITag[]>;
    editTag(dto: IEditTag): Promise<ITag>;
    deleteTag(dto: IDeleteTag): Promise<void>;
}
