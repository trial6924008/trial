export interface ITag {
    id: string;
    productId: string;
    solded: boolean;
    standId: string;
}
