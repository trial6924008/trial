import { ITag } from './ITag';

export interface ICreateTag extends Pick<ITag, 'productId' | 'standId'> {}
