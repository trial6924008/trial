import { ITag } from './ITag';

export interface IGetTag extends Pick<ITag, 'id'> {}
