export interface IStand {
    id: string;
    address: string;
    contacts: string;
    name: string;
    online: boolean;
    tagsCount: number;
}
