import { IStand } from './IStand';

export interface IGetStand extends Pick<IStand, 'id'> {}
