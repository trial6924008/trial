import { ICreateStand } from './ICreateStand';
import { IDeleteStand } from './IDeleteStand';
import { IEditStand } from './IEditStand';
import { IGetStand } from './IGetStand';
import { IStand } from './IStand';

export interface IStandsService {
    createStand(dto: ICreateStand): Promise<IStand>;
    getStand(dto: IGetStand): Promise<IStand>;
    getAllStands(): Promise<IStand[]>;
    editStand(dto: IEditStand): Promise<IStand>;
    deleteStand(dto: IDeleteStand): Promise<void>;
}
