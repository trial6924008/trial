import { IGetStand } from './IGetStand';
import { IStand } from './IStand';

export interface IEditStand
    extends IGetStand,
        Partial<Omit<IStand, 'id' | 'tags'>> {}
