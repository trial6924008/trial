import { IStand } from './IStand';

export interface ICreateStand
    extends Pick<IStand, 'address' | 'contacts' | 'name'> {}
