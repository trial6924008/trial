export enum apiRoutes {
    products = 'api/products',
    stands = 'api/stands',
    tags = 'api/tags',
}
