import { IGetProduct } from './IGetProduct';
import { IProduct } from './IProduct';

export interface IEditProduct
    extends IGetProduct,
        Partial<Omit<IProduct, 'id' | 'tags'>> {}
