import { IProduct } from './IProduct';

export interface IGetProduct extends Pick<IProduct, 'id'> {}
