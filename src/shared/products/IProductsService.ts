import { ICreateProduct } from './ICreateProduct';
import { IDeleteProduct } from './IDeleteProduct';
import { IEditProduct } from './IEditProduct';
import { IGetProduct } from './IGetProduct';
import { IProduct } from './IProduct';

export interface IProductsService {
    createProduct(dto: ICreateProduct): Promise<IProduct>;
    getProduct(dto: IGetProduct): Promise<IProduct>;
    getAllProducts(): Promise<IProduct[]>;
    editProduct(dto: IEditProduct): Promise<IProduct>;
    deleteProduct(dto: IDeleteProduct): Promise<void>;
}
