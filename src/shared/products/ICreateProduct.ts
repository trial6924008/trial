import { IProduct } from './IProduct';

export interface ICreateProduct extends Pick<IProduct, 'name' | 'price'> {}
