import { Module } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { PrismaModule } from './prisma/prisma.module';
import { StandsModule } from './stands/stands.module';
import { ProductsModule } from './products/products.module';
import { TagsModule } from './tags/tags.module';
import { FrontendModule } from './frontend/frontend.module';

@Module({
    imports: [
        ConfigModule,
        PrismaModule,
        ProductsModule,
        StandsModule,
        TagsModule,
        FrontendModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
