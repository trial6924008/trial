import { IsString } from 'class-validator';

export class DatabaseConfigEntity {
    @IsString()
    url: string;
}
