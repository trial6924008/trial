import { ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiConfigEntity } from './api-config.entity';
import { AppConfigEntity } from './app-config.entity';
import { DatabaseConfigEntity } from './database-config.entity';

export class ConfigEntity {
    @ValidateNested()
    @Type(() => ApiConfigEntity)
    api: ApiConfigEntity;

    @ValidateNested()
    @Type(() => AppConfigEntity)
    app: AppConfigEntity;

    @ValidateNested()
    @Type(() => DatabaseConfigEntity)
    database: DatabaseConfigEntity;
}
