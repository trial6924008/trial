import { IsUrl, IsNumber } from 'class-validator';

export class ApiConfigEntity {
    @IsUrl({
        require_port: false,
        require_protocol: false,
        require_tld: false,
        require_valid_protocol: false,
    })
    host: string;

    @IsNumber()
    port: number;
}
