import { IsEnum } from 'class-validator';
import { appModes } from '../enums/app-modes';
import type { appMode } from '../types/app-mode';

export class AppConfigEntity {
    @IsEnum(appModes)
    mode: appMode;
}
