import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiConfigEntity } from '../entities/api-config.entity';

@Injectable()
export class ApiConfigService implements ApiConfigEntity {
    constructor(
        @Inject(ConfigService) private readonly configService: ConfigService,
    ) {}

    get host(): ApiConfigEntity['host'] {
        return this.configService.getOrThrow('api.host');
    }

    get port(): ApiConfigEntity['port'] {
        return this.configService.getOrThrow('api.port');
    }
}
