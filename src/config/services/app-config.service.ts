import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppConfigEntity } from '../entities/app-config.entity';

@Injectable()
export class AppConfigService implements AppConfigEntity {
    constructor(
        @Inject(ConfigService) private readonly configService: ConfigService,
    ) {}

    get mode(): AppConfigEntity['mode'] {
        return this.configService.getOrThrow('app.mode');
    }
}
