import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DatabaseConfigEntity } from '../entities/database-config.entity';

@Injectable()
export class DatabaseConfigService implements DatabaseConfigEntity {
    constructor(
        @Inject(ConfigService) private readonly configService: ConfigService,
    ) {}

    get url(): DatabaseConfigEntity['url'] {
        return this.configService.getOrThrow('database.url');
    }
}
