import { appModes } from '../enums/app-modes';

export type appMode = `${appModes}`;
