import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { ApiConfigService } from './services/api-config.service';
import { AppConfigService } from './services/app-config.service';
import { DatabaseConfigService } from './services/database-config.service';
import { configuration } from './utils/configuration';

@Module({
    imports: [
        NestConfigModule.forRoot({
            load: [configuration],
        }),
    ],
    providers: [ApiConfigService, AppConfigService, DatabaseConfigService],
})
export class ConfigModule {}
