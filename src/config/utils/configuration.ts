import { InternalServerErrorException } from '@nestjs/common';
import { validateSync } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { ConfigEntity } from '../entities/config.entity';
import { AppConfigEntity } from '../entities/app-config.entity';
import { appMode } from '../types/app-mode';
import { ApiConfigEntity } from '../entities/api-config.entity';
import { DatabaseConfigEntity } from '../entities/database-config.entity';

export function configuration(): ConfigEntity {
    const apiConfig: ApiConfigEntity = {
        host: process.env.API_HOST ?? '',
        port: parseInt(process.env.API_PORT ?? '', 10),
    };
    const appConfig: AppConfigEntity = {
        mode: process.env.APP_MODE as appMode,
    };
    const dbConfig: DatabaseConfigEntity = {
        url: process.env.DATABASE_URL ?? '',
    };
    const config: ConfigEntity = {
        api: apiConfig,
        app: appConfig,
        database: dbConfig,
    };

    let entity = plainToClass(ConfigEntity, config);
    const errors = validateSync(entity);

    if (errors.length > 0) {
        throw new InternalServerErrorException(
            errors.map((err) => err.toString()),
        );
    }

    return config;
}
