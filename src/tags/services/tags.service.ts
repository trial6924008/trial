import { Injectable, Inject } from '@nestjs/common';
import { handleError } from '../../core/errors/handle-error';
import { ITagsService } from '../../shared/tags/ITagsService';
import { CreateTagDto } from '../dto/create-tag.dto';
import { DeleteTagDto } from '../dto/delete-tag.dto';
import { EditTagDto } from '../dto/edit-tag.dto';
import { GetTagDto } from '../dto/get-tag.dto';
import { TagEntity } from '../entities/tag.entity';
import { TagsRepository } from '../repositories/tags.repository';

@Injectable()
export class TagsService implements ITagsService {
    constructor(
        @Inject(TagsRepository) private readonly repository: TagsRepository,
    ) {}

    public async createTag(dto: CreateTagDto): Promise<TagEntity> {
        try {
            return await this.repository.create(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async getTag(dto: GetTagDto): Promise<TagEntity> {
        try {
            return await this.repository.readOne(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async getAllTags(): Promise<TagEntity[]> {
        try {
            return await this.repository.readAll();
        } catch (e) {
            throw handleError(e);
        }
    }

    public async editTag(dto: EditTagDto): Promise<TagEntity> {
        try {
            return await this.repository.update(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async deleteTag(dto: DeleteTagDto): Promise<void> {
        try {
            return await this.repository.delete(dto);
        } catch (e) {
            throw handleError(e);
        }
    }
}
