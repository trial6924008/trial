import { IntersectionType } from '@nestjs/swagger';
import { IEditTag } from '../../shared/tags/IEditTag';
import { GetTagDto } from './get-tag.dto';
import { UpdateTagDto } from './update-tag.dto';

export class EditTagDto
    extends IntersectionType(GetTagDto, UpdateTagDto)
    implements IEditTag {}
