import { PartialType, PickType } from '@nestjs/swagger';
import { IEditTag } from '../../shared/tags/IEditTag';
import { TagEntity } from '../entities/tag.entity';

export class UpdateTagDto
    extends PartialType(PickType(TagEntity, ['solded'] as const))
    implements Omit<IEditTag, 'id'> {}
