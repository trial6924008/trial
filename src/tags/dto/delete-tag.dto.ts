import { IDeleteTag } from '../../shared/tags/IDeleteTag';
import { GetTagDto } from './get-tag.dto';

export class DeleteTagDto extends GetTagDto implements IDeleteTag {}
