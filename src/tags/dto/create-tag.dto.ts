import { PickType } from '@nestjs/swagger';
import { ICreateTag } from '../../shared/tags/ICreateTag';
import { TagEntity } from '../entities/tag.entity';

export class CreateTagDto extends PickType(TagEntity, ['productId', 'standId'] as const) implements ICreateTag {}
