import { PickType } from '@nestjs/swagger';
import { IGetTag } from '../../shared/tags/IGetTag';
import { TagEntity } from '../entities/tag.entity';

export class GetTagDto
    extends PickType(TagEntity, ['id'] as const)
    implements IGetTag {}
