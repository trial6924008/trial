import { Injectable, Inject, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { CreateTagDto } from '../dto/create-tag.dto';
import { DeleteTagDto } from '../dto/delete-tag.dto';
import { EditTagDto } from '../dto/edit-tag.dto';
import { GetTagDto } from '../dto/get-tag.dto';
import { TagEntity } from '../entities/tag.entity';

@Injectable()
export class TagsRepository {
    constructor(
        @Inject(PrismaService) private readonly prisma: PrismaService,
    ) {}

    private async checkIdExist(id: string) {
        const result = await this.prisma.tag.findUnique({
            where: {
                id,
            },
            select: {
                id: true,
            },
        });
        return !!result;
    }

    public async readOne(dto: GetTagDto): Promise<TagEntity> {
        const result = await this.prisma.tag.findUnique({
            where: dto,
        });
        if (!result) {
            throw new NotFoundException();
        }
        return result;
    }

    public async readAll(): Promise<TagEntity[]> {
        return await this.prisma.tag.findMany({});
    }

    public async create(dto: CreateTagDto): Promise<TagEntity> {
        return await this.prisma.tag.create({
            data: dto,
        });
    }

    public async update(dto: EditTagDto): Promise<TagEntity> {
        const isExist = await this.checkIdExist(dto.id);
        if (!isExist) {
            throw new NotFoundException();
        }
        const _dto = { ...dto } as Partial<EditTagDto>;
        delete _dto.id;
        const update = _dto as Omit<EditTagDto, 'id'>;
        return await this.prisma.tag.update({
            where: { id: dto.id },
            data: update,
        });
    }

    public async delete(dto: DeleteTagDto): Promise<void> {
        const isExist = await this.checkIdExist(dto.id);
        if (!isExist) {
            throw new NotFoundException();
        }
        await this.prisma.tag.delete({
            where: dto,
        });
    }
}
