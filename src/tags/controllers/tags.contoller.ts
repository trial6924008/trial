import { Inject, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Controller } from '../../core/rest/decorators/controller.decorator';
import { apiRoutes } from '../../shared/common/api-routes';
import { CreateTagDto } from '../dto/create-tag.dto';
import { DeleteTagDto } from '../dto/delete-tag.dto';
import { GetTagDto } from '../dto/get-tag.dto';
import { UpdateTagDto } from '../dto/update-tag.dto';
import { TagEntity } from '../entities/tag.entity';
import { TagsService } from '../services/tags.service';

@Controller(apiRoutes.tags)
export class TagsController {
    constructor(@Inject(TagsService) private readonly service: TagsService) {}

    @Post()
    @ApiOperation({ summary: 'create tag' })
    public async createTag(@Body() dto: CreateTagDto): Promise<TagEntity> {
        return await this.service.createTag(dto);
    }

    @Get(':id')
    @ApiOperation({ summary: 'get tag by ID' })
    public async getTag(@Param() dto: GetTagDto): Promise<TagEntity> {
        return await this.service.getTag(dto);
    }

    @Get()
    @ApiOperation({ summary: 'get all tags' })
    public async getAllTags(): Promise<TagEntity[]> {
        return await this.service.getAllTags();
    }

    @Put(':id')
    @ApiOperation({ summary: 'edit tag' })
    public async editTag(
        @Param() dto: GetTagDto,
        @Body() update: UpdateTagDto,
    ): Promise<TagEntity> {
        return await this.service.editTag({ ...dto, ...update });
    }

    @Delete(':id')
    @ApiOperation({ summary: 'delete tag' })
    public async deleteTag(@Param() dto: DeleteTagDto): Promise<void> {
        return await this.service.deleteTag(dto);
    }
}
