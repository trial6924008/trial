import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsBoolean } from 'class-validator';
import { ITag } from '../../shared/tags/ITag';

export class TagEntity implements ITag {
    @IsMongoId()
    @ApiProperty({ description: 'ID' })
    id: string;

    @IsMongoId()
    @ApiProperty({ description: 'ID of product which tag belongs' })
    productId: string;

    @IsBoolean()
    @ApiProperty({ description: 'Tag status: solded or not' })
    solded: boolean;

    @IsMongoId()
    @ApiProperty({ description: 'ID of Stand where tag is located' })
    standId: string;
}
