import { Injectable, Inject } from '@nestjs/common';
import { handleError } from '../../core/errors/handle-error';
import { IStandsService } from '../../shared/stands/IStandsService';
import { CreateStandDto } from '../dto/create-stand.dto';
import { DeleteStandDto } from '../dto/delete-stand.dto';
import { EditStandDto } from '../dto/edit-stand.dto';
import { GetStandDto } from '../dto/get-stand.dto';
import { StandEntity } from '../entities/stand.entity';
import { StandsRepository } from '../repositories/stands.repository';

@Injectable()
export class StandsService implements IStandsService {
    constructor(
        @Inject(StandsRepository) private readonly repository: StandsRepository,
    ) {}

    public async createStand(dto: CreateStandDto): Promise<StandEntity> {
        try {
            return await this.repository.create(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async getStand(dto: GetStandDto): Promise<StandEntity> {
        try {
            return await this.repository.readOne(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async getAllStands(): Promise<StandEntity[]> {
        try {
            return await this.repository.readAll();
        } catch (e) {
            throw handleError(e);
        }
    }

    public async editStand(dto: EditStandDto): Promise<StandEntity> {
        try {
            return await this.repository.update(dto);
        } catch (e) {
            throw handleError(e);
        }
    }

    public async deleteStand(dto: DeleteStandDto): Promise<void> {
        try {
            return await this.repository.delete(dto);
        } catch (e) {
            throw handleError(e);
        }
    }
}
