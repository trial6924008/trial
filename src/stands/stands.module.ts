import { Module } from '@nestjs/common';
import { PrismaModule } from '../prisma/prisma.module';
import { StandsController } from './controllers/stands.contoller';
import { StandsRepository } from './repositories/stands.repository';
import { StandsService } from './services/stands.service';

@Module({
    imports: [PrismaModule],
    controllers: [StandsController],
    providers: [StandsService, StandsRepository],
})
export class StandsModule {}
