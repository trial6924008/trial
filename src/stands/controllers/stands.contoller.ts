import { Inject, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Controller } from '../../core/rest/decorators/controller.decorator';
import { apiRoutes } from '../../shared/common/api-routes';
import { CreateStandDto } from '../dto/create-stand.dto';
import { DeleteStandDto } from '../dto/delete-stand.dto';
import { GetStandDto } from '../dto/get-stand.dto';
import { UpdateStandDto } from '../dto/update-stand.dto';
import { StandEntity } from '../entities/stand.entity';
import { StandsService } from '../services/stands.service';

@Controller(apiRoutes.stands)
export class StandsController {
    constructor(
        @Inject(StandsService) private readonly service: StandsService,
    ) {}

    @Post()
    @ApiOperation({ summary: 'create stand' })
    public async createStand(
        @Body() dto: CreateStandDto,
    ): Promise<StandEntity> {
        return await this.service.createStand(dto);
    }

    @Get(':id')
    @ApiOperation({ summary: 'get stand by ID' })
    public async getStand(@Param() dto: GetStandDto): Promise<StandEntity> {
        return await this.service.getStand(dto);
    }

    @Get()
    @ApiOperation({ summary: 'get all stands' })
    public async getAllStands(): Promise<StandEntity[]> {
        return await this.service.getAllStands();
    }

    @Put(':id')
    @ApiOperation({ summary: 'edit stand' })
    public async editStand(
        @Param() dto: GetStandDto,
        @Body() update: UpdateStandDto,
    ): Promise<StandEntity> {
        return await this.service.editStand({ ...dto, ...update });
    }

    @Delete(':id')
    @ApiOperation({ summary: 'delete stand' })
    public async deleteStand(@Param() dto: DeleteStandDto): Promise<void> {
        return await this.service.deleteStand(dto);
    }
}
