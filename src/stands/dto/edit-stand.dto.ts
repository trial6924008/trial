import { IntersectionType } from '@nestjs/swagger';
import { IEditStand } from '../../shared/stands/IEditStand';
import { GetStandDto } from './get-stand.dto';
import { UpdateStandDto } from './update-stand.dto';

export class EditStandDto
    extends IntersectionType(GetStandDto, UpdateStandDto)
    implements IEditStand {}
