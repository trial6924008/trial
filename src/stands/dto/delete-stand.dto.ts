import { IDeleteStand } from '../../shared/stands/IDeleteStand';
import { GetStandDto } from './get-stand.dto';

export class DeleteStandDto extends GetStandDto implements IDeleteStand {}
