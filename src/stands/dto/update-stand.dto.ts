import { OmitType, PartialType } from '@nestjs/swagger';
import { IEditStand } from '../../shared/stands/IEditStand';
import { StandEntity } from '../entities/stand.entity';

export class UpdateStandDto
    extends PartialType(OmitType(StandEntity, ['id'] as const))
    implements Omit<IEditStand, 'id'> {}
