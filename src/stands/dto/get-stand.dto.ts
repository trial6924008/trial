import { PickType } from '@nestjs/swagger';
import { IGetStand } from '../../shared/stands/IGetStand';
import { StandEntity } from '../entities/stand.entity';

export class GetStandDto
    extends PickType(StandEntity, ['id'] as const)
    implements IGetStand {}
