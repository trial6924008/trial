import { PickType } from '@nestjs/swagger';
import { ICreateStand } from '../../shared/stands/ICreateStand';
import { StandEntity } from '../entities/stand.entity';

export class CreateStandDto
    extends PickType(StandEntity, ['address', 'contacts', 'name'] as const)
    implements ICreateStand {}
