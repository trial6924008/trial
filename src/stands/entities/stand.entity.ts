import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsString, MinLength, IsBoolean, IsNumber } from 'class-validator';
import { IStand } from '../../shared/stands/IStand';

export class StandEntity implements IStand {
    @IsMongoId()
    @ApiProperty({ description: 'ID' })
    id: string;

    @IsString()
    @ApiProperty({ description: 'Address of stand' })
    address: string;

    @IsString()
    @ApiProperty({
        description: "Contacts of shop's manager where stand is installed",
    })
    contacts: string;

    @IsString()
    @MinLength(1)
    @ApiProperty({ description: 'Name of stand' })
    name: string;

    @IsBoolean()
    @ApiProperty({ description: 'Stand status: online/offline' })
    online: boolean;

    @IsNumber()
    @ApiProperty({ description: 'Number of tags on the stand' })
    tagsCount: number;
}
