import {
    Injectable,
    Inject,
    ConflictException,
    NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { CreateStandDto } from '../dto/create-stand.dto';
import { DeleteStandDto } from '../dto/delete-stand.dto';
import { EditStandDto } from '../dto/edit-stand.dto';
import { GetStandDto } from '../dto/get-stand.dto';
import { StandEntity } from '../entities/stand.entity';

@Injectable()
export class StandsRepository {
    constructor(
        @Inject(PrismaService) private readonly prisma: PrismaService,
    ) {}

    private async checkIdExist(id: string) {
        const result = await this.prisma.stand.findUnique({
            where: {
                id,
            },
            select: {
                id: true,
            },
        });
        return !!result;
    }

    private async checkNameExist(name: string) {
        const result = await this.prisma.stand.findUnique({
            where: {
                name,
            },
            select: {
                id: true,
            },
        });
        return !!result;
    }

    private async countTags(dto: GetStandDto) {
        return await this.prisma.tag.count({
            where: {
                standId: dto.id,
                solded: false
            }
        });
    }

    public async readOne(dto: GetStandDto): Promise<StandEntity> {
        const result = await this.prisma.stand.findUnique({
            where: dto,
        });
        if (!result) {
            throw new NotFoundException();
        }
        const tagsCount = await this.countTags(dto);
        return {
            ...result,
            tagsCount
        };
    }

    public async readAll(): Promise<StandEntity[]> {
        const result = await this.prisma.stand.findMany({});
        return Promise.all(result.map((async (item) => {
            const tagsCount = await this.countTags({ id: item.id });
            return {
                ...item,
                tagsCount
            }
        })));
    }

    public async create(dto: CreateStandDto): Promise<StandEntity> {
        const isExist = await this.checkNameExist(dto.name);
        if (isExist) {
            throw new ConflictException();
        }
        const result = await this.prisma.stand.create({
            data: dto,
        });
        return {
            ...result,
            tagsCount: 0
        };
    }

    public async update(dto: EditStandDto): Promise<StandEntity> {
        const isIdExist = await this.checkIdExist(dto.id);
        if (!isIdExist) {
            throw new NotFoundException();
        }
        if (dto.name) {
            const isNameExist = await this.checkNameExist(dto.name);
            if (isNameExist) {
                throw new ConflictException();
            }
        }
        const _dto = { ...dto } as Partial<EditStandDto>;
        delete _dto.id;
        const update = _dto as Omit<EditStandDto, 'id'>;
        const result = await this.prisma.stand.update({
            where: { id: dto.id },
            data: update,
        });
        const tagsCount = await this.countTags(dto)
        return {
            ...result,
            tagsCount,
        }
    }

    public async delete(dto: DeleteStandDto): Promise<void> {
        const isExist = await this.checkIdExist(dto.id);
        if (!isExist) {
            throw new NotFoundException();
        }
        await this.prisma.stand.delete({
            where: dto,
        });
    }
}
