import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { handleError } from '../errors/handle-error';

@Injectable()
export class ValidationPipe implements PipeTransform {
    private toValidate(metatype: Function): boolean {
        const types: Function[] = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }

    public async transform(value: any, { metatype }: ArgumentMetadata) {
        try {
            if (!value || typeof value !== 'object') {
                throw new BadRequestException('body is not an object');
            }
            if (!metatype || !this.toValidate(metatype)) {
                return value;
            }
            const object = plainToClass(metatype, value);
            const errors = await validate(object);
            if (errors.length > 0) {
                const reason = errors
                    .map(({ property }) => property)
                    .filter((property, i, arr) => arr.indexOf(property) === i)
                    .map((property) => `${property} field is not valid`);
                throw new BadRequestException(...reason);
            }
            return value;
        } catch (e) {
            throw handleError(e);
        }
    }
}
