import { Controller as BaseController, applyDecorators, UsePipes } from '@nestjs/common';
import {
    ApiTags,
    ApiBadRequestResponse,
    ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import { errorsDescriptions } from '../enums/errors-descriptions.enum';
import { apiRoutes } from '../../../shared/common/api-routes';
import { ValidationPipe } from '../../validation/validation.pipe';

export function Controller(path: apiRoutes) {
    return applyDecorators(
        BaseController({
            path,
        }),
        UsePipes(new ValidationPipe()),
        ApiTags(path),
        ApiBadRequestResponse({ description: errorsDescriptions.badRequest }),
        ApiInternalServerErrorResponse({
            description: errorsDescriptions.serverErr,
        }),
    );
}
