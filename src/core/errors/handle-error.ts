import {
    BadRequestException,
    ConflictException,
    InternalServerErrorException,
    NotFoundException,
} from '@nestjs/common';

export function handleError(e: unknown) {
    console.debug(e);
    const isCustomErr =
        e instanceof BadRequestException ||
        e instanceof ConflictException ||
        e instanceof NotFoundException ||
        e instanceof InternalServerErrorException
    if (isCustomErr) {
        return e;
    }
    return new InternalServerErrorException();
}
